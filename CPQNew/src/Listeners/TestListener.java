package Listeners;

import com.relevantcodes.extentreports.LogStatus;

import Driver.DriverHelper;
import Driver.DriverTestcase;
import Driver.Log;
import Driver.PropertyReader;
import Driver.ScreenRecorder;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import Reporter.ExtentManager;
import Reporter.ExtentTestManager;
import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;

 
public class TestListener extends DriverTestcase implements ITestListener  {
 
	ScreenRecorder recordTest;
	String Filename;
	ATUTestRecorder recorder;
	PropertyReader pr=new PropertyReader();
    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
   
    //Before starting all tests, below method runs.
    public void onStart(ITestContext iTestContext) {
        Log.info("I am on Start method " + iTestContext.getName());
        
        iTestContext.setAttribute("WebDriver", this.getwebdriver());
        
      
       //---------------------------------------------------------------------
        System.out.println("Driver instance in Listemer"+this.getwebdriver());
    }
 
    //After ending all tests, below method runs.
    public void onFinish(ITestContext iTestContext) {
        Log.info("I am on Finish method " + iTestContext.getName());
        //Do tier down operations for extentreports reporting!
      // ======================Realese Recording Video ==============================
       
       
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
    public void onTestStart(ITestResult iTestResult) {
        Log.info("I am on TestStart method " +  getTestMethodName(iTestResult) + " start");
        //Start operation for extentreports.
        //ExtentTestManager.
        Log.info("I am in onStart method " + iTestResult.getTestContext().getAttribute("testName"));
        DateFormat df = new SimpleDateFormat("yyyyMMdd-HHmm");
        Filename=iTestResult.getName()+ "-" + df.format(new Date());
      //  Filename=iTestResult.getTestContext().getAttribute("testName").toString();
        System.out.println("Test name from Exfel"+Filename);
//        //=======================Start Recording==========================
      
        //======================================================
      
 
			//To start video recording.
			
        
        
        ExtentTestManager.startTest(iTestResult.getTestContext().getSuite().getXmlSuite().getName().toString()+"-"+iTestResult.getTestContext().getCurrentXmlTest().getName().toString()+"-"+iTestResult.getTestContext().getAttribute("testName").toString(),"");
    }
 
    public void onTestSuccess(ITestResult iTestResult) {
        Log.info("I am on TestSuccess method " +  getTestMethodName(iTestResult) + " succeed");
        //Extentreports log operation for passed tests.
      ExtentTestManager.getTest().log(LogStatus.PASS, getTestMethodName(iTestResult)+" : Test Method has been passed");
      //ExtentTestManager.endTest();
      
      //=======================Stop Recording Recording==========================
      
      //======================================================
     
      Object testClass = iTestResult.getInstance();
      WebDriver webDriver = ((DriverTestcase) testClass).getwebdriver();
      webDriver.close();
      //extent.endTest(logger);
      ExtentTestManager.endTest();
      ExtentManager.getReporter().flush();
    }
 
    public void onTestFailure(ITestResult iTestResult) {
       // Log.info("I am on TestFailure method " +  getTestMethodName(iTestResult) + " failed");
        //iTestResult.setStatus(0);
        //Get driver from BaseTest and assign to local webdriver variable.
    ExtentTestManager.getTest().log(LogStatus.FAIL, getTestMethodName(iTestResult)+" : Test Method has been Failed");
        Object testClass = iTestResult.getInstance();
        //iTestResult.setStatus(arg0);
        WebDriver webDriver = ((DriverTestcase) testClass).getwebdriver();
       
        //Take base64Screenshot screenshot.
        String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)getwebdriver()).
                getScreenshotAs(OutputType.BASE64);
        String Message=iTestResult.getThrowable().getMessage() != null ? iTestResult.getThrowable().getMessage().toString() :
        	iTestResult.getThrowable().getCause().toString();
      
        //====================================
       //Full page screenshot
        String screenshot ="";
        try {
        	screenshot =	DriverHelper.Capturefullscreenshot(getwebdriver());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

        //===============================================================
        
        System.out.println("Result Messages"+Message);
        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.getTest().log(LogStatus.FAIL,Message+
               /* ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot) +*/
                ExtentTestManager.getTest().addBase64ScreenShot(screenshot));
        
        
        //=======================Stop Recording and Doenload video on Extent Report==========================
       
        
        webDriver.close();
        System.out.println("3243223423423423423");
        ExtentTestManager.endTest();
        System.out.println("2222222342342342342324323423");
        ExtentManager.getReporter().flush();
    }
 
    public void onTestError(ITestResult iTestResult) {
        Log.info("I am on TestSuccess method " +  getTestMethodName(iTestResult) + " errored");
        //Extentreports log operation for passed tests.
        	ExtentTestManager.getTest().log(LogStatus.ERROR, getTestMethodName(iTestResult)+" : Test Method has been errored");
            Object testClass = iTestResult.getInstance();
            //iTestResult.setStatus(arg0);
            WebDriver webDriver = ((DriverTestcase) testClass).getwebdriver();
     
            //Take base64Screenshot screenshot.
            String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)getwebdriver()).
                    getScreenshotAs(OutputType.BASE64);
            
            //=======================Stop Recording and Doenload video on Extent Report==========================
            webDriver.close();
            ExtentTestManager.endTest();
            ExtentManager.getReporter().flush();
            //Extentreports log and screenshot operations for failed tests.
          //  ExtentTestManager.getTest().log(LogStatus.ERROR,"Test Errored",
           //         ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
    }

    public void onTestSkipped(ITestResult iTestResult) {
        Log.info("I am in onTestSkipped method "+  getTestMethodName(iTestResult) + " skipped");
        //Extentreports log operation for skipped tests.
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
    
        
        //Full page screenshot
        String screenshot ="";
        try {
        	screenshot =	DriverHelper.Capturefullscreenshot(getwebdriver());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

        //===============================================================
        
      
        //Extentreports log and screenshot operations for failed tests.
        ExtentTestManager.getTest().log(LogStatus.SKIP,
               /* ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot) +*/
                ExtentTestManager.getTest().addBase64ScreenShot(screenshot));
        
        //=======================Stop Recording and Doenload video on Extent Report==========================
       
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        Log.info("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
        
        
        //=======================Stop Recording and Doenload video on Extent Report==========================
       
        ExtentTestManager.endTest();
        ExtentManager.getReporter().flush();
    }
 
}