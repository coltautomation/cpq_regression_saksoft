package ScriptHelper;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Driver.DriverHelper;
import Driver.xmlreader;

public class ExploreHelper extends DriverHelper {

	WebElement el;
	xmlreader xml = new xmlreader("src\\Locators\\Explore.xml");

	public ExploreHelper(WebDriver parentdriver) {
		super(parentdriver);

	}

	public void NavigatetoExplore() throws Exception {
		System.out.println("Waiting Started");
		Thread.sleep(10000);
		System.out.println("Waiting End");
		openurl2(Getkeyvalue("Explore_URL"));

	}

	public void ExploreWorkflownearnet(Object[][] inputdata) throws InterruptedException, Exception {
		List data = RequestIDNearnet.get();
		for (int i = 0; i < data.size(); i++) {
			Object[] newdata = (Object[]) data.get(i);
			System.out.println("Size Foe Each line item" + newdata.length);
			System.out.println("A Site for lineitem" + i + " is " + newdata[0].toString());
			System.out.println("B Site for lineitem" + i + " is " + newdata[1].toString());
		}
		for (int i = 0; i < data.size(); i++) {
			switch (inputdata[i][14].toString()) {
			case "Approve": {
				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					if (!newdata[j].toString().equals("")) {
						System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
						WaitforElementtobeclickable(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue"));
						Clickon(getwebelement(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue")));
						WaitforElementtobeclickable(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
						Clickon(getwebelement(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
						Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
						Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
						WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMeNearnet"));
						Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMeNearnet")));
						Thread.sleep(5000);
						// String a =Getattribute(getwebelement("//label[text()='Request
						// Type']/following::span"),"value");
						String a = Gettext(getwebelement("//label[text()='Request Type']/following::span"));
						System.out.println(a);
						if (a.equals("BCP Revalidation")) {
							WaitforElementtobeclickable(xml.getlocator("//locators/QuoteValiduptomonths"));
							SendKeys(getwebelement(xml.getlocator("//locators/QuoteValiduptomonths")), "5");
						} else {

							try {
								if (isElementPresent(xml.getlocator("//locators/PrimaryDistancefromColtNetwork"))) {
									WaitforElementtobeclickable(
											xml.getlocator("//locators/PrimaryDistancefromColtNetwork"));
									Clickon(getwebelement(xml.getlocator("//locators/PrimaryDistancefromColtNetwork")));
									SendKeys(getwebelement(xml.getlocator("//locators/PrimaryDistancefromColtNetwork")),
											"18");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/ExploreNearNetDistance"));
									Clickon(getwebelement(xml.getlocator("//locators/ExploreNearNetDistance")));
									SendKeys(getwebelement(xml.getlocator("//locators/ExploreNearNetDistance")), "18");
								}
							} catch (Exception e) {
								if (isElementPresent(xml.getlocator("//locators/SecondaryDistancefromColtNetwork"))) {
									WaitforElementtobeclickable(
											xml.getlocator("//locators/SecondaryDistancefromColtNetwork"));
									Clickon(getwebelement(
											xml.getlocator("//locators/SecondaryDistancefromColtNetwork")));
									SendKeys(
											getwebelement(
													xml.getlocator("//locators/SecondaryDistancefromColtNetwork")),
											"18");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/ExploreNearNetDistance"));
									Clickon(getwebelement(xml.getlocator("//locators/ExploreNearNetDistance")));
									SendKeys(getwebelement(xml.getlocator("//locators/ExploreNearNetDistance")), "18");
								}

							}

							try {
								if (isElementPresent(xml.getlocator("//locators/PrimaryQuoteValidupto"))) {
									WaitforElementtobeclickable(xml.getlocator("//locators/PrimaryQuoteValidupto"));
									Clickon(getwebelement(xml.getlocator("//locators/PrimaryQuoteValidupto")));
									SendKeys(getwebelement(xml.getlocator("//locators/PrimaryQuoteValidupto")), "8");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/QuoteValiduptomonths"));
									Clickon(getwebelement(xml.getlocator("//locators/QuoteValiduptomonths")));
									SendKeys(getwebelement(xml.getlocator("//locators/QuoteValiduptomonths")), "8");
								}
							} catch (Exception e) {
								if (isElementPresent(xml.getlocator("//locators/SecondaryQuoteValidupto"))) {
									WaitforElementtobeclickable(xml.getlocator("//locators/SecondaryQuoteValidupto"));
									Clickon(getwebelement(xml.getlocator("//locators/SecondaryQuoteValidupto")));
									SendKeys(getwebelement(xml.getlocator("//locators/SecondaryQuoteValidupto")), "8");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/QuoteValiduptomonths"));
									Clickon(getwebelement(xml.getlocator("//locators/QuoteValiduptomonths")));
									SendKeys(getwebelement(xml.getlocator("//locators/QuoteValiduptomonths")), "8");
								}
							}

							try {
								if (isElementPresent(xml.getlocator("//locators/Primarycurrencyquote"))) {
									WaitforElementtobeclickable(xml.getlocator("//locators/Primarycurrencyquote"));
									Clickon(getwebelement(xml.getlocator("//locators/Primarycurrencyquote")));
									SendKeys(getwebelement(xml.getlocator("//locators/Primarycurrencyquote")), "EUR");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/ExploreNearNetCurrency"));
									Clickon(getwebelement(xml.getlocator("//locators/ExploreNearNetCurrency")));
									SendKeys(getwebelement(xml.getlocator("//locators/ExploreNearNetCurrency")), "EUR");
								}
								Thread.sleep(2000);
							} catch (Exception e) {
								if (isElementPresent(xml.getlocator("//locators/Secondarycurrencyquote"))) {
									WaitforElementtobeclickable(xml.getlocator("//locators/Secondarycurrencyquote"));
									Clickon(getwebelement(xml.getlocator("//locators/Secondarycurrencyquote")));
									SendKeys(getwebelement(xml.getlocator("//locators/Secondarycurrencyquote")), "EUR");
								} else {
									WaitforElementtobeclickable(xml.getlocator("//locators/ExploreNearNetCurrency"));
									Clickon(getwebelement(xml.getlocator("//locators/ExploreNearNetCurrency")));
									SendKeys(getwebelement(xml.getlocator("//locators/ExploreNearNetCurrency")), "EUR");
								}
								Thread.sleep(2000);

							}

							Thread.sleep(3000);
							try {
								WaitforElementtobeclickable(xml.getlocator("//locators/NearNetAddCost"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetAddCost")));
							} catch (Exception e) {
								WaitforElementtobeclickable(xml.getlocator("//locators/NearNetAddCost2"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetAddCost2")));
							}
//							SendKeys(getwebelement(xml.getlocator("//locators/NearNetAddCost")),"EUR");
//							WaitforElementtobeclickable(xml.getlocator("//locators/NearNetAddCost"));
//							Clickon(getwebelement(xml.getlocator("//locators/NearNetAddCost")));
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/NearNetCostCatogory"));
							Clickon(getwebelement(xml.getlocator("//locators/NearNetCostCatogory")));
							Thread.sleep(1000);
							EnterText("Construction");
							EnterText2(Keys.ENTER);
							// SendKeys(getwebelement(xml.getlocator("//locators/NearNetCostCatogory")),"Construction");
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/NearNetSubCostCatogory"));
							Clickon(getwebelement(xml.getlocator("//locators/NearNetSubCostCatogory")));
							Thread.sleep(1000);
							EnterText("WayLeave");
							EnterText2(Keys.ENTER);
							// SendKeys(getwebelement(xml.getlocator("//locators/NearNetSubCostCatogory")),"WayLeave");
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/NearNetUnitCost"));
							Clickon(getwebelement(xml.getlocator("//locators/NearNetUnitCost")));
							Thread.sleep(1000);
							EnterText("100");
							EnterText2(Keys.ENTER);
							// SendKeys(getwebelement(xml.getlocator("//locators/NearNetUnitCost")),"100");
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/NearNetCostQuantity"));
							Clickon(getwebelement(xml.getlocator("//locators/NearNetCostQuantity")));
							Thread.sleep(1000);
							EnterText("1");
							EnterText2(Keys.ENTER);
							// SendKeys(getwebelement(xml.getlocator("//locators/NearNetCostQuantity")),"1");
							Thread.sleep(2000);
							if (isElementPresent(xml.getlocator("//locators/Dualentrydigcosttab"))) {
								WaitforElementtobeclickable(xml.getlocator("//locators/Dualentrydigcosttab"));
								Clickon(getwebelement(xml.getlocator("//locators/Dualentrydigcosttab")));
								Thread.sleep(2000);
								WaitforElementtobeclickable(
										xml.getlocator("//locators/SecondaryDistancefromColtNetwork"));
								Clickon(getwebelement(xml.getlocator("//locators/SecondaryDistancefromColtNetwork")));
								SendKeys(getwebelement(xml.getlocator("//locators/SecondaryDistancefromColtNetwork")),
										"18");
								Thread.sleep(3000);
								WaitforElementtobeclickable(xml.getlocator("//locators/NearNetAddCost2"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetAddCost2")));

								Thread.sleep(2000);
								WaitforElementtobeclickable(
										xml.getlocator("//locators/NearNetCostCatogoryfordualentry"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetCostCatogoryfordualentry")));
								Thread.sleep(1000);
								EnterText("Construction");
								EnterText2(Keys.ENTER);
								// SendKeys(getwebelement(xml.getlocator("//locators/NearNetCostCatogory")),"Construction");
								Thread.sleep(2000);
								WaitforElementtobeclickable(
										xml.getlocator("//locators/NearNetSubCostCatogoryfordualentry"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetSubCostCatogoryfordualentry")));
								Thread.sleep(1000);
								EnterText("WayLeave");
								EnterText2(Keys.ENTER);
								// SendKeys(getwebelement(xml.getlocator("//locators/NearNetSubCostCatogory")),"WayLeave");
								Thread.sleep(2000);
								WaitforElementtobeclickable(xml.getlocator("//locators/NearNetUnitCostfordualentry"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetUnitCostfordualentry")));
								Thread.sleep(1000);
								EnterText("100");
								EnterText2(Keys.ENTER);
								// SendKeys(getwebelement(xml.getlocator("//locators/NearNetUnitCost")),"100");
								Thread.sleep(2000);
								WaitforElementtobeclickable(
										xml.getlocator("//locators/NearNetCostQuantityfordualentry"));
								Clickon(getwebelement(xml.getlocator("//locators/NearNetCostQuantityfordualentry")));
								Thread.sleep(1000);
								EnterText("1");
								EnterText2(Keys.ENTER);
								// SendKeys(getwebelement(xml.getlocator("//locators/NearNetCostQuantity")),"1");
								Thread.sleep(2000);

							}
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/SendtoSalesNearNet"));
							Clickon(getwebelement(xml.getlocator("//locators/SendtoSalesNearNet")));
							Thread.sleep(2000);
							Clickon(getwebelement(xml.getlocator("//locators/Backtolist")));
							Thread.sleep(2000);

						}
					}

					else {
						System.out.println("None of Data update required");
					}

				}

				break;
			}
			case "Dig Cost not Possible": {
				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
					WaitforElementtobeclickable(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue"));
					Clickon(getwebelement(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue")));
					WaitforElementtobeclickable(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
					Clickon(getwebelement(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//	            Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
					Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
					WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMeNearnet"));
					Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMeNearnet")));
					Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/Digcodenotpossible"));
					Clickon(getwebelement(xml.getlocator("//locators/Digcodenotpossible")));
					WaitforElementtobeclickable(xml.getlocator("//locators/SendtoSalesNearNet"));
					Clickon(getwebelement(xml.getlocator("//locators/SendtoSalesNearNet")));
					break;
				}
			}
			case "Sigle Feed": {
				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
					WaitforElementtobeclickable(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue"));
					Clickon(getwebelement(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue")));
					WaitforElementtobeclickable(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
					Clickon(getwebelement(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//	            Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
					Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
					WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMeNearnet"));
					Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMeNearnet")));
					Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/SingleFeed"));
					Clickon(getwebelement(xml.getlocator("//locators/SingleFeed")));
					WaitforElementtobeclickable(xml.getlocator("//locators/SendtoSalesNearNet"));
					Clickon(getwebelement(xml.getlocator("//locators/SendtoSalesNearNet")));
					break;
				}
			}
			case "Dual Feed": {
				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
					WaitforElementtobeclickable(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue"));
					Clickon(getwebelement(xml.getlocator("//locators/NearNetFibrePlanningWorkQueue")));
					WaitforElementtobeclickable(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
					Clickon(getwebelement(
							xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//	            Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
					Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
					WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMeNearnet"));
					Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMeNearnet")));
					Thread.sleep(5000);
					WaitforElementtobeclickable(xml.getlocator("//locators/DualFeed"));
					Clickon(getwebelement(xml.getlocator("//locators/DualFeed")));
					WaitforElementtobeclickable(xml.getlocator("//locators/SendtoSalesNearNet"));
					Clickon(getwebelement(xml.getlocator("//locators/SendtoSalesNearNet")));
					break;
				}
			}
			default: {
				break;
			}
			}
		}

	}

	public void ExploreWorkflow(Object[][] inputdata) throws InterruptedException, Exception {
		List data = RequestID.get();
		for (int i = 0; i < data.size(); i++) {
			Object[] newdata = (Object[]) data.get(i);
			System.out.println("Size Foe Each line item" + newdata.length);
			System.out.println("A Site for lineitem" + i + " is " + newdata[0].toString());
			System.out.println("B Site for lineitem" + i + " is " + newdata[1].toString());
		}
		for (int i = 0; i < data.size(); i++) {
			switch (inputdata[i][14].toString()) {

			case "Approve": {
				// List data=RequestID.get();
				// data=RequestID.get();

				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					if (!newdata[j].toString().equals("")) {

						System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
						WaitforElementtobeclickable(xml.getlocator("//locators/ONQTWorkQuoteLink"));
						Clickon(getwebelement(xml.getlocator("//locators/ONQTWorkQuoteLink")));
						WaitforElementtobeclickable(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
						Clickon(getwebelement(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//		            WaitforElementtobeclickable(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190527115421"));

//					Clickon(getwebelement(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190527115421")));
//					System.out.println("workflow");
						// Thread.sleep(2000);
						waitForpageloadExplore();
						WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
						Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
						WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMe"));
						Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMe")));
						waitForpageloadExplore();
						String b = Getattribute(
								getwebelement("//label[text()='Response Req']/following::td[1]/div//input"), "value");
						WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
						WaitforElementtobeclickable(xml.getlocator("//locators/CreateCost"));
						Clickon(getwebelement(xml.getlocator("//locators/CreateCost")));
						waitForpageloadExplore();
						String a = Getattribute(
								getwebelement("//label[text()='Conversion Type']/following::td[1]/div//input"),
								"value");

						System.out.println(a);
						System.out.println(a);
						if (a.equals("Revalidation")) {
							WaitforElementtobeclickable(xml.getlocator("//locators/QuoteValidity"));
							SendKeys(getwebelement(xml.getlocator("//locators/QuoteValidity")), "45");
						} else if (a.equals("Renegotiation")) {
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC")),
									"600");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC")),
									"600");
						} else if (b.contains("DSL & OLO")) {
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier")),
									"Colt");
							SendkeaboardKeys(
									getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "Node")),
									Keys.ARROW_DOWN);
							Thread.sleep(3000);
							// Clickon(getwebelement(xml.getlocator("//locators/NodeDropdown")));
							Clickon(getwebelement(xml.getlocator("//locators/NodeDropdown")));
							// SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
							// "Node")),Keys.ENTER);
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC")),
									"500");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC")),
									"500");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Term (Years)"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Term (Years)")), "1");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Currency"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Currency")),
									"EUR");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Price Type"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Price Type")),
									"ACTUAL");
							WaitforElementtobeclickable(xml.getlocator("//locators/Filedsname").replace("Fieldname",
									"Connector/Interface"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
									"Connector/Interface")), "100BaseT");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Access Technology"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Access Technology")),
									"ETH");

							if (isElementPresent(xml.getlocator("//locators/Dualentryforexlopre"))) {
								WaitforElementtobeclickable(xml.getlocator("//locators/Filedsname").replace("Fieldname",
										"Dual Entry from OLO"));
								SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
										"Dual Entry from OLO")), "Cost Included");
							}
							WaitforElementtobeclickable(xml.getlocator("//locators/CreateCostSubmit"));
							Clickon(getwebelement(xml.getlocator("//locators/CreateCostSubmit")));

							Thread.sleep(6000);
							WaitforElementtobeclickable(xml.getlocator("//locators/ApproveQuoteButton"));
							Clickon(getwebelement(xml.getlocator("//locators/ApproveQuoteButton")));
							Thread.sleep(6000);
							Thread.sleep(2000);
							WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
							Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
							WaitforElementtobeclickable(xml.getlocator("//locators/CreateCost"));
							Clickon(getwebelement(xml.getlocator("//locators/CreateCost")));
							Thread.sleep(2000);
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier")),
									"Colt");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier Prod Name"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier Prod Name")),
									"ULL/EFM");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Maximum Bandwidth"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Maximum Bandwidth")),
									"100 Mbps");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Service Available"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Service Available")),
									"Y");

						} else if (a.equals("DSL")) {
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier")),
									"Colt");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Product Name"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Product Name")),
									"ULL EFM");

						} else {
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier")),
									"Colt");
							Thread.sleep(3000);
							SendkeaboardKeys(
									getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "Node")),
									Keys.ARROW_DOWN);
							Thread.sleep(3000);
							// Clickon(getwebelement(xml.getlocator("//locators/NodeDropdown")));
							Clickon(getwebelement(xml.getlocator("//locators/NodeDropdown")));
							// SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
							// "Node")),Keys.ARROW_DOWN);

							// SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
							// "Node")),Keys.ENTER);
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "NRC")),
									"500");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "MRC")),
									"500");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Term (Years)"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Term (Years)")), "1");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Currency"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Currency")),
									"EUR");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Price Type"));
							SendKeys(
									getwebelement(
											xml.getlocator("//locators/Filedsname").replace("Fieldname", "Price Type")),
									"ACTUAL");
							WaitforElementtobeclickable(xml.getlocator("//locators/Filedsname").replace("Fieldname",
									"Connector/Interface"));
							SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
									"Connector/Interface")), "100BaseT");
							WaitforElementtobeclickable(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Access Technology"));
							SendKeys(getwebelement(
									xml.getlocator("//locators/Filedsname").replace("Fieldname", "Access Technology")),
									"ETH");
							if (isElementPresent(xml.getlocator("//locators/Dualentryforexlopre"))) {
								WaitforElementtobeclickable(xml.getlocator("//locators/Filedsname").replace("Fieldname",
										"Dual Entry from OLO"));
								SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname",
										"Dual Entry from OLO")), "Cost Included");
							}

//					//WaitforElementtobeclickable(xml.getlocator("//locators/CarrierDropdown"));
//					//Clickon(getwebelement(xml.getlocator("//locators/CarrierDropdown")));
//					//WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "Colt"));
//					//Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "Colt")));
//					//WaitforElementtobeclickable(xml.getlocator("//locators/NodeDropdown"));
//					//Clickon(getwebelement(xml.getlocator("//locators/NodeDropdown")));
//					//WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "Graz"));
//					
//					//Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "Graz")));
//					SendKeys(getwebelement(xml.getlocator("//locators/NRCInput")),"500");
//					SendKeys(getwebelement(xml.getlocator("//locators/MRCInput")),"500");
//					WaitforElementtobeclickable(xml.getlocator("//locators/TermDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/TermDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "1"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "1")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/CurrencyDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/CurrencyDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "EUR"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "EUR")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/PriceTypeDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/PriceTypeDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "ACTUAL"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "ACTUAL")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/ConnectorDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/ConnectorDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "100BaseT"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "100BaseT")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/AccessTechnologyDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/AccessTechnologyDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "ETH"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "ETH")));
						}
						WaitforElementtobeclickable(xml.getlocator("//locators/CreateCostSubmit"));
						Clickon(getwebelement(xml.getlocator("//locators/CreateCostSubmit")));
						Thread.sleep(6000);

						if (b.contains("DSL & OLO")) {
							WaitforElementtobeclickable(xml.getlocator("//locators/SaveCost"));
							Clickon(getwebelement(xml.getlocator("//locators/SaveCost")));
							Thread.sleep(6000);
							WaitforElementtobeclickable(xml.getlocator("//locators/ApproveforDSL"));
							Clickon(getwebelement(xml.getlocator("//locators/ApproveforDSL")));

						}
//					WaitforElementtobeclickable(xml.getlocator("//locators/TypeDropdown"));
//					Clickon(getwebelement(xml.getlocator("//locators/TypeDropdown")));
//					WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "Port"));
//					
//					Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "Port")));
//					SendKeys(getwebelement(xml.getlocator("//locators/NameInput")),"ABC");
//					SendKeys(getwebelement(xml.getlocator("//locators/InputNRCAdditional")),"500");
//					SendKeys(getwebelement(xml.getlocator("//locators/InputMRCAdditional")),"500");

						WaitforElementtobeclickable(xml.getlocator("//locators/ApproveQuoteButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ApproveQuoteButton")));
						Thread.sleep(6000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
						WaitforElementtobeclickable(xml.getlocator("//locators/ChangeStatus"));
						Clickon(getwebelement(xml.getlocator("//locators/ChangeStatus")));
						WaitforElementtobeclickable(xml.getlocator("//locators/CloseRequest"));
						Clickon(getwebelement(xml.getlocator("//locators/CloseRequest")));
						WaitforElementtobeclickable(xml.getlocator("//locators/BacktoQuote"));
						Clickon(getwebelement(xml.getlocator("//locators/BacktoQuote")));
						/*
						 * WaitforElementtobeclickable(xml.getlocator("//locators/MenuItem"));
						 * Clickon(getwebelement(xml.getlocator("//locators/MenuItem")));
						 * WaitforElementtobeclickable(xml.getlocator("//locators/Logout"));
						 * Clickon(getwebelement(xml.getlocator("//locators/Logout")));
						 */
					}

					else {
						System.out.println("No related Explore Request Raised");
					}

				}

				break;
			}
			case "Reject": {

				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					if (!newdata[j].toString().equals("")) {

						System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
						WaitforElementtobeclickable(xml.getlocator("//locators/ONQTWorkQuoteLink"));
						Clickon(getwebelement(xml.getlocator("//locators/ONQTWorkQuoteLink")));
						WaitforElementtobeclickable(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
						Clickon(getwebelement(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//			            WaitforElementtobeclickable(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190526084256"));

//						Clickon(getwebelement(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190526084256")));
//						System.out.println("workflow");
						// Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
						Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
						WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMe"));
						Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMe")));
						// Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
						WaitforElementtobeclickable(xml.getlocator("//locators/Reject"));
						Clickon(getwebelement(xml.getlocator("//locators/Reject")));
						SendKeys(getwebelement(xml.getlocator("//locators/AddMessageTextArea")),
								"This ONQT Work is Rejected by Automation");
						Clickon(getwebelement(xml.getlocator("//locators/AddMessageSubmit")));
						WaitforElementtobeclickable(xml.getlocator("//locators/BacktoQuote"));
						Clickon(getwebelement(xml.getlocator("//locators/BacktoQuote")));
						// Thread.sleep(5000);

						/*
						 * WaitforElementtobeclickable(xml.getlocator("//locators/MenuItem"));
						 * Clickon(getwebelement(xml.getlocator("//locators/MenuItem")));
						 * WaitforElementtobeclickable(xml.getlocator("//locators/Logout"));
						 * Clickon(getwebelement(xml.getlocator("//locators/Logout")));
						 */
					}

					else {
						System.out.println("No related Explore Request Raised");
					}

				}

				break;
			}
			case "Supplier No Bid": {

				Object[] newdata = (Object[]) data.get(i);
				for (int j = 0; j < newdata.length; j++) {
					if (!newdata[j].toString().equals("")) {

						System.out.println("A Site for lineitem" + i + " is " + newdata[j].toString());
						WaitforElementtobeclickable(xml.getlocator("//locators/ONQTWorkQuoteLink"));
						Clickon(getwebelement(xml.getlocator("//locators/ONQTWorkQuoteLink")));
						WaitforElementtobeclickable(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString()));
						Clickon(getwebelement(
								xml.getlocator("//locators/SearchQuoteId").replace("value", newdata[j].toString())));
//			            WaitforElementtobeclickable(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190527115421"));

//						Clickon(getwebelement(xml.getlocator("//locators/SearchQuoteId").replace("value", "20190527115421")));
//						System.out.println("workflow");
						// Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ButtonAction"));
						Clickon(getwebelement(xml.getlocator("//locators/ButtonAction")));
						WaitforElementtobeclickable(xml.getlocator("//locators/AssignRequestToMe"));
						Clickon(getwebelement(xml.getlocator("//locators/AssignRequestToMe")));
						// Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
						WaitforElementtobeclickable(xml.getlocator("//locators/CreateCost"));
						Clickon(getwebelement(xml.getlocator("//locators/CreateCost")));
						// Thread.sleep(5000);

						WaitforElementtobeclickable(
								xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier"));
						SendKeys(getwebelement(xml.getlocator("//locators/Filedsname").replace("Fieldname", "Carrier")),
								"Colt");
//						WaitforElementtobeclickable(xml.getlocator("//locators/CarrierDropdown"));
//						Clickon(getwebelement(xml.getlocator("//locators/CarrierDropdown")));
//						WaitforElementtobeclickable(xml.getlocator("//locators/Selectvalue").replace("value", "Colt"));
//						Clickon(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "Colt")));

						WaitforElementtobeclickable(xml.getlocator("//locators/CreateSupplierNoBidButton"));
						Clickon(getwebelement(xml.getlocator("//locators/CreateSupplierNoBidButton")));
						// Thread.sleep(5000);
						WaitforElementtobeclickable(xml.getlocator("//locators/ActionButton"));
						Clickon(getwebelement(xml.getlocator("//locators/ActionButton")));
						WaitforElementtobeclickable(xml.getlocator("//locators/ChangeStatus"));
						Clickon(getwebelement(xml.getlocator("//locators/ChangeStatus")));
						WaitforElementtobeclickable(xml.getlocator("//locators/CloseRequest"));
						Clickon(getwebelement(xml.getlocator("//locators/CloseRequest")));
						WaitforElementtobeclickable(xml.getlocator("//locators/BacktoQuote"));
						Clickon(getwebelement(xml.getlocator("//locators/BacktoQuote")));
						/*
						 * WaitforElementtobeclickable(xml.getlocator("//locators/MenuItem"));
						 * Clickon(getwebelement(xml.getlocator("//locators/MenuItem")));
						 * WaitforElementtobeclickable(xml.getlocator("//locators/Logout"));
						 * Clickon(getwebelement(xml.getlocator("//locators/Logout")));
						 */
					}

					else {
						System.out.println("No related Explore Request Raised");
					}

					break;
				}

				System.out.println("Completed the Process start sleeping for couple of second to data trasmited");
				Thread.sleep(50000);
				System.out.println("Ended the waite time");

			}

			}
		}
	}
}
