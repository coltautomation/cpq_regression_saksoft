package ScriptHelper;

import java.io.IOException;
import java.util.Random;

import org.dom4j.DocumentException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.ThreadedRefreshHandler;
import com.relevantcodes.extentreports.LogStatus;
import Driver.DriverHelper;
import Driver.xmlreader;
import Reporter.ExtentTestManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class GenralInfoHelper extends DriverHelper{
	
	
	
	WebElement el;
	xmlreader xml=new xmlreader("src\\Locators\\GeneralInfo.xml");
	xmlreader xml2=new xmlreader("src\\Locators\\Configuration.xml");
	public GenralInfoHelper(WebDriver parentdriver)
	{
		super(parentdriver);
		
	}


	public void GenralInfomration(Object[][] Inputdata) throws Exception {
		//WaitforCPQloader();
		waitForpageload();
//  Commented for Unit Testing		
//		Clickon(getwebelement(xml2.getlocator("//locators/saveQuote")));
		waitForpageload();
		Expandthesection(getwebelement(xml.getlocator("//locators/SectionName").replace("Sectionname", "Legal Contact Details")),getwebelement(xml.getlocator("//locators/Clickableelemt").replace("Sectionname", "Legal Contact Details")));
//		safeJavaScriptClick(getwebelement(xml.getlocator("//locators/LegalcontactGetdetail")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/Selectionpopup").replace("Popupbox", "Legal Contact Details"));
		
//		Code developed by Vasanth 
		selectContactsFromPrompt("legalGetContacts");
		
		waitForpageload();
		
//		WaitforElementtobeclickable(xml.getlocator("//locators/Selectprimarycontact"));
//		Clickon(getwebelement(xml.getlocator("//locators/Selectprimarycontact")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/SubmitContact"));
//		Clickon(getwebelement(xml.getlocator("//locators/SubmitContact")));
//		waitForpageload();
		
//		AddContactPerson();
		Thread.sleep(5000);	
		Expandthesection(getwebelement(xml.getlocator("//locators/SectionName").replace("Sectionname", "Technical/Ordering Contacts")),getwebelement(xml.getlocator("//locators/Clickableelemt").replace("Sectionname", "Technical/Ordering Contacts")));
//		Code developed by Vasanth 
		selectContactsFromPrompt("technicalGetContacts");
		
		//safeJavaScriptClick(getwebelement(xml.getlocator("//locators/TechnicalcontactGetdetail")));
		//Clickon(getwebelement(xml.getlocator("//locators/TechnicalcontactGetdetail")));
		//waitandForElementDisplay(xml.getlocator("//locators/Selectprimarycontact"),1);
		//Thread.sleep(3000);
//		WaitforElementtobeclickable(xml.getlocator("//locators/Selectionpopup").replace("Popupbox", "Technical/Ordering Contacts"));
//		waitForpageload();
//		
//		WaitforElementtobeclickable(xml.getlocator("//locators/Selectprimarycontact"));
//		Clickon(getwebelement(xml.getlocator("//locators/Selectprimarycontact")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/SubmitContact"));
//		Clickon(getwebelement(xml.getlocator("//locators/SubmitContact")));
//		waitForpageload();

//		AddContactPerson();
		Thread.sleep(5000);
       Expandthesection(getwebelement(xml.getlocator("//locators/SectionName").replace("Sectionname", "Opportunity Info")),getwebelement(xml.getlocator("//locators/Clickableelemt").replace("Sectionname", "Opportunity Info")));
		
		DealClass.set(GetValueofInput(getwebelement(xml.getlocator("//locators/Dealclass"))));
		System.out.println("Deal class after Updating all added products is"+DealClass.get());
	}
	
	public void AddContactPerson() throws InterruptedException, IOException, DocumentException
	{
		//Random number for unique Contact details
		 String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
	        StringBuilder salt = new StringBuilder();
	       Random rnd = new Random();
	        while (salt.length() < 5) 
	       { // length of the random string.
	            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
	            salt.append(SALTCHARS.charAt(index));
	       }
	     String saltStr = salt.toString();
	       System.out.println(saltStr);
	     //Add Contact Deatils with Random String  
	       
	     
	      
	    WaitforElementtobeclickable(xml.getlocator("//locators/createContactBtn"));
		Clickon(getwebelement(xml.getlocator("//locators/createContactBtn")));   
	    waitForpageload();  
	//    Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	    WaitforElementtobeclickable(xml.getlocator("//locators/SelectCheckbox"));
		Clickon(getwebelement(xml.getlocator("//locators/SelectCheckbox")));     
	//	 Thread.sleep(1000);
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/TitleField"));
		Clickon(getwebelement(xml.getlocator("//locators/TitleField"))); 
	//	 Thread.sleep(4000);
	//	waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		 javascriptexecutor(getwebelement(xml.getlocator("//locators/Selecttitle").replace("Title", "Mr.")));
		WaitforElementtobeclickable(xml.getlocator("//locators/Selecttitle").replace("Title", "Mr."));
		Clickon(getwebelement(xml.getlocator("//locators/Selecttitle").replace("Title", "Mr."))); 
		
		//Enter First Name
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/PersonFirstName"));
		Clickon(getwebelement(xml.getlocator("//locators/PersonFirstName"))); 
		String FirstName = "Ashwani" +saltStr;
		EnterText(FirstName );
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(1000);
		EnterText2(Keys.ENTER);
		
		//Enter Last Name
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/PersonLastName"));
		Clickon(getwebelement(xml.getlocator("//locators/PersonLastName")));   
		EnterText("Singh");
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(1000);
		EnterText2(Keys.ENTER);
				   
		//Enter Email
		waitForpageload();  
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/PersonEmailId"));
		Clickon(getwebelement(xml.getlocator("//locators/PersonEmailId")));   
		EnterText("Ashwani"+saltStr+"@yopmail.com");
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		//Thread.sleep(1000);
		EnterText2(Keys.ENTER);    
		
		//Enter Mobile
		waitForpageload();  
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/PersonMobile"));
		Clickon(getwebelement(xml.getlocator("//locators/PersonMobile")));   
		EnterText("+449858585958");
	//	Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(1000);
		EnterText2(Keys.ENTER); 
		
		//Enter TelePhone
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/PersonTelePhone"));
		Clickon(getwebelement(xml.getlocator("//locators/PersonTelePhone")));   
		EnterText("+4487598476");
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(1000);
		EnterText2(Keys.ENTER);
		
		//Select Correspond Languagae
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/CorrespondLanguage"));
		Clickon(getwebelement(xml.getlocator("//locators/CorrespondLanguage"))); 
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	 Thread.sleep(1000);
		waitForpageload();  
		WaitforElementtobeclickable(xml.getlocator("//locators/SelectLanguage").replace("Language", "French"));
		Clickon(getwebelement(xml.getlocator("//locators/SelectLanguage").replace("Language", "French"))); 
		
		//Select Preferred Contact type
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		javascriptexecutor(getwebelement(xml.getlocator("//locators/PreferredContact")));
		WaitforElementtobeclickable(xml.getlocator("//locators/PreferredContact"));
		Clickon(getwebelement(xml.getlocator("//locators/PreferredContact"))); 
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(1000);
		WaitforElementtobeclickable(xml.getlocator("//locators/SelectPreferredcontact").replace("Preferred", "Email"));
		Clickon(getwebelement(xml.getlocator("//locators/SelectPreferredcontact").replace("Preferred", "Email"))); 
	//	Thread.sleep(1000);
		waitForpageload();  
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/Submitbuttonforadd"));
		Clickon(getwebelement(xml.getlocator("//locators/Submitbuttonforadd"))); 
		waitForpageload();
//		Thread.sleep(20000);
		
		
		
		
		
		
		
		//---------------------------------------------------------
		
		
		
		
////		 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
//		WaitforElementtobeclickable(xml.getlocator("//locators/CleatSearchButton"));
//		Clickon(getwebelement(xml.getlocator("//locators/CleatSearchButton"))); 
//		waitForpageload();
//	//	Thread.sleep(30000);	
//	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
//		WaitforElementtobeclickable(xml.getlocator("//locators/ContactfirstName"));
//		SendKeys(getwebelement(xml.getlocator("//locators/ContactfirstName")), FirstName);
//	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
//		waitForpageload();  
//		WaitforElementtobeclickable(xml.getlocator("//locators/ContactLastName"));
//		SendKeys(getwebelement(xml.getlocator("//locators/ContactLastName")), "Singh");
////		Thread.sleep(3000);
////		 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
//		waitForpageload();  
//		WaitforElementtobeclickable(xml.getlocator("//locators/SearchContact"));
//		Clickon(getwebelement(xml.getlocator("//locators/SearchContact"))); 
//		waitForpageload();
////		Thread.sleep(3000);
//		int time = 0;
//		do
//		{
//			Thread.sleep(5000);
//			time=time+1;
//			WaitforElementtobeclickable(xml.getlocator("//locators/SearchContact"));
//			Clickon(getwebelement(xml.getlocator("//locators/SearchContact"))); 
//			waitForpageload();
//			WaitforElementtobeclickable(xml.getlocator("//locators/VerifyFirstName").replace("FirstName", FirstName));
//		}
//		while(isElementPresent(xml.getlocator("//locators/VerifyFirstName").replace("FirstName", FirstName)) || time>20);
		
		
		
//		if(isElementPresent(xml.getlocator("//locators/VerifyFirstName").replace("FirstName", FirstName)))
//		{
//			System.out.println("Ready to Select Contact!!!!");
//		}
//		else {
//			WaitforElementtobeclickable(xml.getlocator("//locators/SearchContact"));
//			Clickon(getwebelement(xml.getlocator("//locators/SearchContact"))); 
//			waitForpageload();
//			WaitforElementtobeclickable(xml.getlocator("//locators/VerifyFirstName").replace("FirstName", FirstName));
//		}
//		Thread.sleep(2000);
	//	 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	waitForpageload();  
		WaitforElementtobeclickable(xml.getlocator("//locators/Selectprimarycontact"));
		Clickon(getwebelement(xml.getlocator("//locators/Selectprimarycontact")));
		WaitforElementtobeclickable(xml.getlocator("//locators/SubmitContact"));
		Clickon(getwebelement(xml.getlocator("//locators/SubmitContact")));
		waitForpageload();
//		Thread.sleep(5000);
//		 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		WaitforElementtobeclickable(xml.getlocator("//locators/saveQuote"));
		Clickon(getwebelement(xml.getlocator("//locators/saveQuote")));
		waitForpageload();
//		 Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
	//	Thread.sleep(5000);
		
	}
	public void AdminChanges(Object[][] Inputdata) throws  Exception
	{
		if(Inputdata[0][72].toString().equals("Yes"))
		{
		Clickon(getwebelement(xml.getlocator("//locators/ReviseCommercial")));
		Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
		
		for(int i=0; i<Inputdata.length;i++)
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/ModelSelector").replace("index", String.valueOf(i+1)));
			Clickon(getwebelement(xml.getlocator("//locators/ModelSelector").replace("index", String.valueOf(i+1))));
			WaitforElementtobeclickable(xml.getlocator("//locators/Reconfigure"));
			Clickon(getwebelement(xml.getlocator("//locators/Reconfigure")));
			Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
			
			Getmaploaded(xml.getlocator("//locators/GoogleMapifram"), xml.getlocator("//locators/Messages"));
			
			
			switch(Inputdata[i][13].toString())
			{
			case "ContractTermUpdate" :
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/SiteDetailTab"));
				Clickon(getwebelement(xml.getlocator("//locators/SiteDetailTab")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				Select(getwebelement(xml.getlocator("//locators/Contractterm")), Inputdata[i][7].toString());
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting for Loading to be completed");
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/UpdateButton"));
				Clickon(getwebelement(xml.getlocator("//locators/UpdateButton")));
				break;
			}
			case "OOH":
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/FeaturesTab"));
				Clickon(getwebelement(xml.getlocator("//locators/FeaturesTab")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/FeatureName").replace("value", "outsideBusinessHoursInstallationAEnd"));
				Clickon(getwebelement(xml.getlocator("//locators/FeatureName").replace("value", "outsideBusinessHoursInstallationAEnd")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/FeatureName").replace("value", "outsideBusinessHoursInstallationBEnd"));
				Clickon(getwebelement(xml.getlocator("//locators/FeatureName").replace("value", "outsideBusinessHoursInstallationBEnd")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/UpdateButton"));
				Clickon(getwebelement(xml.getlocator("//locators/UpdateButton")));
				break;
			}
			case "CRD" :
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/FeaturesTab"));
				Clickon(getwebelement(xml.getlocator("//locators/FeaturesTab")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		        Calendar c = Calendar.getInstance();
		        
		        c.add(Calendar.DATE, +7);
		      
		        Date modifiedDate = c.getTime();
		        String RequiredDate = simpleDateFormat.format(modifiedDate);
		        System.out.println(RequiredDate);
		        WaitforElementtobeclickable(xml.getlocator("//locators/CustomerRequiredDate"));
				SendKeys(getwebelement(xml.getlocator("//locators/CustomerRequiredDate")),RequiredDate);
				WaitforElementtobeclickable(xml.getlocator("//locators/UpdateButton"));
				Clickon(getwebelement(xml.getlocator("//locators/UpdateButton")));
				break;
		        
			}
			case "FastTrack" :
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/FeaturesTab"));
				Clickon(getwebelement(xml.getlocator("//locators/FeaturesTab")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/FeatureName").replace("value", "fasttrackAddon"));
				Clickon(getwebelement(xml.getlocator("//locators/FeatureName").replace("value", "fasttrackAddon")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/UpdateButton"));
				Clickon(getwebelement(xml.getlocator("//locators/UpdateButton")));
				break;
			}
			case "Pro-ActiveManagementContacts" :
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/AdditionalProductDataTab"));
				Clickon(getwebelement(xml.getlocator("//locators/AdditionalProductDataTab")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				WaitforElementtobeclickable(xml.getlocator("//locators/FeatureName").replace("value", "proactiveManagementContact_Qto"));
				Clickon(getwebelement(xml.getlocator("//locators/FeatureName").replace("value", "proactiveManagementContact_Qto")));
				Getloadingcomplete(xml.getlocator("//locators/LoadingDailog"));
				Select(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "title_qto")), "Mr.");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "firstName_qto")), "Automation");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "lastName_Qto")), "Test");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "contactNumber_Qto")), "+44123456789");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "mobileNumber_qto")), "+44123456789");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "email_Qto")), "abc@email.com");
				SendKeys(getwebelement(xml.getlocator("//locators/InputValue").replace("value", "faxNumber_qto")), "12345678");
				Select(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "correspondanceLanguage_qto")), "English-American");
				Select(getwebelement(xml.getlocator("//locators/Selectvalue").replace("value", "preferredContactMethod_qto")), "Phone");
				WaitforElementtobeclickable(xml.getlocator("//locators/UpdateButton"));
				Clickon(getwebelement(xml.getlocator("//locators/UpdateButton")));
				break;
			}
			
			}
			
			WaitforElementtobeclickable(xml.getlocator("//locators/SaveButton"));
			Clickon(getwebelement(xml.getlocator("//locators/SaveButton")));
			waitForpageload();
			WaitforElementtobeclickable(xml.getlocator("//locators/ApprovalTab"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Scroll the Page to Top");
			javascriptexecutor(getwebelement(xml.getlocator("//locators/ApprovalTab")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Approval Tab");
			Clickon(getwebelement(xml.getlocator("//locators/ApprovalTab")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Waiting For Loading to be Completed");
			WaitforElementtobeclickable(xml.getlocator("//locators/SubmitetoApproval"));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Submite for Approval");
			Clickon(getwebelement(xml.getlocator("//locators/SubmitetoApproval")));
			waitForpageload();
			Thread.sleep(2000);
			//waitForpageload();
			WaitforElementtobeclickable(xml.getlocator("//locators/SaleuserSubmited"));
			
		}
		
	  }
	}	
}
