package ScriptHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.DocumentException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;
import com.relevantcodes.extentreports.LogStatus;

import Driver.BrokenLinkSetUp;
import Driver.DriverHelper;
import Driver.xmlreader;
import Reporter.ExtentTestManager;

public class C4CHelper extends DriverHelper{
	
	
	
	WebElement el;
	xmlreader xml=new xmlreader("src\\Locators\\C4C.xml");
	xmlreader xml2=new xmlreader("src\\Locators\\Configuration.xml");
	xmlreader xml3=new xmlreader("src\\Locators\\Container.xml");
	xmlreader xml4=new xmlreader("src\\Locators\\Login.xml");
//	xmlreader xml5=new xmlreader("src\\Locators\\DisscountandAprroval.xml");
	public C4CHelper(WebDriver parentdriver)
	{
		super(parentdriver);
	}
	public void NavigatetoC4C() throws Exception
	{
		Geturl(Getkeyvalue("C4C_URL"));
	
	}
	// Code For Account Navigation
	public void Movetoaccount(Object[][] Inputdata) throws Exception
	{
	//	Assert.fail("Manual Failed");
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Customer Section");
		////////Thread.sleep(10000);
		if(!Inputdata[0][0].toString().equals("")) {
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/CustomerSection"));
		Thread.sleep(1000);
		Clickon(getwebelement(xml.getlocator("//locators/CustomerSection")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
		WaitforElementtobeclickable(xml.getlocator("//locators/Accountlink"));
//		checkbrokenlink();
//		Assert.fail();
		Clickon(getwebelement(xml.getlocator("//locators/Accountlink")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		//////Thread.sleep(5000);
		try {
		WaitforElementtobeclickable(xml.getlocator("//locators/AccountsDropdown"));
		Clickon(getwebelement(xml.getlocator("//locators/AccountsDropdown")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
	
		}
		catch(Exception e)
		{   WaitforElementtobeclickable(xml.getlocator("//locators/AccountsDropdown"));
		    Clickon(getwebelement(xml.getlocator("//locators/AccountsDropdown")));
		    WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			
		}
		WaitforElementtobeclickable(xml.getlocator("//locators/SelectAll"));
		Clickon(getwebelement(xml.getlocator("//locators/SelectAll")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Account link in navidation");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/AccountSearchField"));
		Search(Inputdata[0][0].toString(),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search For Account number"+Inputdata[0][0].toString());
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/OCN").replace("Accountnumber", Inputdata[0][0].toString()));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/OCN").replace("Accountnumber", Inputdata[0][0].toString())));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Account Name");
		WaitforElementtobeclickable(xml.getlocator("//locators/customer"));
		}
		else
		{
			// Price Lookup without Quote
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/SalesSection"));
			Clickon(getwebelement(xml.getlocator("//locators/SalesSection")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
			WaitforElementtobeclickable(xml.getlocator("//locators/Quotelink"));
			
			Clickon(getwebelement(xml.getlocator("//locators/Quotelink")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		////////Thread.sleep(2000);
	}

	public void Product_Add() throws Exception
	{
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/ProductTab"));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/ProductTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Tab in Account details");
		//Click on New to add Opportunity  Bandwidth
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/AddProductlink"));
		
		
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/AddProductlink")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New button to create New Opportunuity");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "Product"));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Product")),"");
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Product")),"SD WAN");
		//////Thread.sleep(2000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Product")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Opportunuity Name");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "Contract Length In Months"));
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Contract Length In Months")),"");
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Contract Length In Months")),"12");
		//////Thread.sleep(2000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Contract Length In Months")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "Install and Setup"));
		getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Install and Setup")).clear();
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Install and Setup")),"500");
		//////Thread.sleep(2000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Install and Setup")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "Bandwidth"));
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Bandwidth")),"");
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Bandwidth")),"10 MBPS");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Bandwidth")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "A End City"));
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "A End City")),"");
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "A End City")),"London");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "A End City")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "B End City"));
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "B End City")),"");
		Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "B End City")),"London");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "B End City")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Fields").replace("FName", "Cloud Service Provider"));
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Cloud Service Provider")),"");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Cloud Service Provider")),"IBM");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Product/Fields").replace("FName", "Cloud Service Provider")),Keys.ENTER);
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/AddProductlink"));
		
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/AddProductlink")));
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New button to create New Opportunuity");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Product/Save"));
		
		
		Clickon(getwebelement(xml.getlocator("//locators/Product/Save")));
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New button to create New Opportunuity");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
	//Code for Opportunity Add
	public void Opportunity_Add(Object[][] Inputdata) throws Exception
	{
	  	//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/OpportunuityTab"));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/OpportunuityTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Tab in Account details");
		//Click on New to add Opportunity
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Newlink"));
		
		
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Newlink")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New button to create New Opportunuity");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(10000);NewOpportubutytab
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/NewOpportubutytab"));
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Name"));
		SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/Name")),"Auto_opty3");
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Opportunity/Name")),Keys.ENTER);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Opportunuity Name");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Offer"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/Offer")),"New Business");
		
		
		
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Offer")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Offer"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "New Business")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Offer type");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryProgramme"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryProgramme")),"Business Development");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryProgramme")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryProgramme"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Business Development")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Primary Program");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryAttribute"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryAttribute")),"Cisco");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryAttribute")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryAttribute"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Cisco")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter PrimaryAttribute");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency"))," ");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency")),"US Dollar");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Singapore Dollar")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Currency");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/More"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/More")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SaveOpenOpty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SaveOpenOpty")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),3);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save and Open Opportunuity Option");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Clickmore"));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Clickmore")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link to Expand the Details");
    	WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Thread.sleep(6000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Editopty"));
		//safeJavaScriptClick(getwebelement(xml.getlocator("//locators/Opportunity/Editopty")));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Editopty")));
		
		//Enter Sales Unit detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SalesUnit"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")),"Colt");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SalesUnit"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")),"Colt"); 
		//Enter Legal Complexity detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(10000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit Option to Edit the Details");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Lcomplexity"));
		SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")),"");
		//////Thread.sleep(1000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")),"Standard");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")));
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		if(!(Inputdata[0][15].equals("BeSpoke")||Inputdata[0][15].equals("NonStandard")||Inputdata[0][47].equals("Yes"))) {
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Standard")));
		
		//Enter Technical Complexity detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Legal Complexity");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Tcomplexity"));
		//SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")),"1");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")));
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "1")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		}
		
		else {
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Non-Standard")));
			
			//Enter Technical Complexity detail
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Legal Complexity");
			WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Tcomplexity"));
			//SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")),"1");
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "2")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			try
			{
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SolutionPartner")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Yes")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			}catch(Exception e)
			{
				System.out.println("Solution partner not required");
			}
			//////Thread.sleep(3000);
		}
		
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Techincal Complexity");
			System.out.println("In The Step Click on Save");
			WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/ClickSaveOpty"));
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/ClickSaveOpty")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save Detail");
			WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OptyId"));
			OpportunityID.set(Gettext(getwebelement(xml.getlocator("//locators/Opportunity/OptyId"))));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Newly Created Opportunuity ID is"+OpportunityID.get().toString());
			System.out.println(OpportunityID.get());
//			Logout();
//			LoginAsC4CAddmin("C4C");
////			 if(Inputdata[0][79].toString().equalsIgnoreCase("Yes")) {
////		    	LaunchURLAgain("C4C");
////		     }
//			OpenOpportunutiDetail() ;
//			UpdateOpportunuty(Inputdata);
//			Logout();
//			LoginAsC4CSales("C4C");
////			 if(Inputdata[0][79].toString().equalsIgnoreCase("Yes")) {
////			    	LaunchURLAgain("C4C");
////			     }
			OpenOpportunutiDetail() ;
		
	}
	public void OpenOpportunutiDetail() throws Exception
	
	{
		WaitforElementtobeclickable(xml.getlocator("//locators/SalesSection"));
		Clickon(getwebelement(xml.getlocator("//locators/SalesSection")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
		WaitforElementtobeclickable(xml.getlocator("//locators/Salseopportunuty"));
		
		Clickon(getwebelement(xml.getlocator("//locators/Salseopportunuty")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/AccountsDropdown"));
		Thread.sleep(2000);
		Clickon(getwebelement(xml.getlocator("//locators/AccountsDropdown")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/SelectAll"));
		
		Clickon(getwebelement(xml.getlocator("//locators/SelectAll")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Search(OpportunityID.get().toString(),1);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Ooprtnuitylink").replace("Opportunitinumber", OpportunityID.get().toString()));
		Clickon(getwebelement(xml.getlocator("//locators/Ooprtnuitylink").replace("Opportunitinumber", OpportunityID.get().toString())));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clikc on Quote ID"+OpportunityID.get().toString());
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
	}
//	public void UpdateOpportunuty() throws Exception
//	{
//		
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save and Open Opportunuity Option");
//		//WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Clickmore"));
//		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
//		//Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Clickmore")));
//		Thread.sleep(2000);
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link to Expand the Details");
//		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
//		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Editopty"));
//		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Editopty")));
//		
//		//Enter Sales Unit detail
//		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
//		
//		//WaitforElementtobeclickable(xml.getlocator("//locators/RequestGoclosure"));
//		//Clickon(getwebelement(xml.getlocator("//locators/RequestGoclosure")));
//		
//		//Enter Sales Unit detail
//		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//		if(isElementPresent(xml.getlocator("//locators/SalseApproved")))
//		{
//		WaitforElementtobeclickable(xml.getlocator("//locators/SalseApproved"));
//		Clickon(getwebelement(xml.getlocator("//locators/SalseApproved")));
//		
//		//Enter Sales Unit detail
//		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//		WaitforElementtobeclickable(xml.getlocator("//locators/SolutionApproved"));
//		Clickon(getwebelement(xml.getlocator("//locators/SolutionApproved")));
//		}
//		else
//		{
//			WaitforElementtobeclickable(xml.getlocator("//locators/GovernanceTab"));
//			Clickon(getwebelement(xml.getlocator("//locators/GovernanceTab")));
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SalesApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go0SalesApproved")));
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SolutionApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go0SolutionApproved")));
//			
//			if(isElementPresent(xml.getlocator("//locators/Go0Outcome")))
//			{
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go0Outcome"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go0Outcome")));
//			Thread.sleep(2000);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes0"));
//			Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes0")));
//		    }
//			try {
//				if(isElementPresent(xml.getlocator("//locators/Go1Outcome")))
//				{
//				
//				WaitforElementtobeclickable(xml.getlocator("//locators/Go1Outcome"));
//				SendKeys(getwebelement(xml.getlocator("//locators/Go1Outcome")), "01 - Approved to proceed");
//				Thread.sleep(2000);
//				WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes1"));
//				Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes1")));
//				}}
//				catch(Exception e)
//				{
//					System.out.println("Unable to find GO 1 OutComes");
//				}
//			Thread.sleep(2000);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go1check"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go1check")));
//			Thread.sleep(2000);
//			 DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
//			
//			
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go1Date"));
//			SendKeys(getwebelement(xml.getlocator("//locators/Go1Date")),  df.format(new Date()));
//			Thread.sleep(2000);
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SalesApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go1SalesApproved")));
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SolutionApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go1SolutionApproved")));
//			
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SalesApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go2SalesApproved")));
//			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SolutionApproved"));
//			Clickon(getwebelement(xml.getlocator("//locators/Go2SolutionApproved")));
//	
//			
//		}
//		//Enter Sales Unit detail
//		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/ClickSaveOpty"));
//		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/ClickSaveOpty")));
//		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//		
//	}
	public void UpdateOpportunuty(Object[][] Inputdata) throws Exception
	{
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save and Open Opportunuity Option");

//		
		Thread.sleep(2000);
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Editopty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Editopty")));
		
		//Enter Sales Unit detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
		
		//WaitforElementtobeclickable(xml.getlocator("//locators/RequestGoclosure"));
		//Clickon(getwebelement(xml.getlocator("//locators/RequestGoclosure")));
		
		//Enter Sales Unit detail
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		System.out.println(Inputdata[0][15].toString()+" "+Inputdata[0][15].toString().equals(null)+" and "+ Inputdata[0][15].toString().length());
		if(isElementPresent(xml.getlocator("//locators/SalseApproved")))
		{
//			//Kashyap
			if(Inputdata[0][79].toString().equalsIgnoreCase("Yes")){
				WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Clickmore"));
				WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),2);
				Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Clickmore")));
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link to Expand the Details");

			}
		WaitforElementtobeclickable(xml.getlocator("//locators/SalseApproved"));
		Clickon(getwebelement(xml.getlocator("//locators/SalseApproved")));
		
		//Enter Sales Unit detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/SolutionApproved"));
		Clickon(getwebelement(xml.getlocator("//locators/SolutionApproved")));
		
		}
		else if(Inputdata[0][15].toString().length()>0) {
			System.out.println("Entered in Se Update condition");
			WaitforElementtobeclickable(xml.getlocator("//locators/GovernanceTab"));
			Clickon(getwebelement(xml.getlocator("//locators/GovernanceTab")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			//G0check
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0check"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0check")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0SolutionApproved")));
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0Date"));
			SendKeys(getwebelement(xml.getlocator("//locators/Go0Date")),  df.format(new Date()));
			Thread.sleep(2000);
			
			if(isElementPresent(xml.getlocator("//locators/Go0Outcome")))
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/Go0Outcome"));
				Clickon(getwebelement(xml.getlocator("//locators/Go0Outcome")));
				Thread.sleep(2000);
				WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes0"));
				Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes0")));
				
			}
			//go1 check
			try {
				if(isElementPresent(xml.getlocator("//locators/Go1Outcome")))
				{
				
				WaitforElementtobeclickable(xml.getlocator("//locators/Go1Outcome"));
				SendKeys(getwebelement(xml.getlocator("//locators/Go1Outcome")), "01 - Approved to proceed");
				Thread.sleep(2000);
				WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes1"));
				Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes1")));
				}}
				catch(Exception e)
				{
					System.out.println("Unable to find GO 1 OutComes");
				}
			Thread.sleep(2000);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1check"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1check")));
			Thread.sleep(2000);
			
			
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1Date"));
			SendKeys(getwebelement(xml.getlocator("//locators/Go1Date")),  df.format(new Date()));
			Thread.sleep(2000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1SolutionApproved")));
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1FinanceApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1FinanceApproved")));
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1LegalApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1LegalApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			
			//Go2 Check
			if(isElementPresent(xml.getlocator("//locators/Go2Outcome")))
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/Go2Outcome"));
				SendKeys(getwebelement(xml.getlocator("//locators/Go2Outcome")), "02 - Approved to submit");
				Thread.sleep(2000);
//				WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes2"));
//				Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes2")));
				
			}
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2check"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2check")));
			Thread.sleep(2000);
			 
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2Date"));
			System.out.println(df.format(new Date()));
			SendKeys(getwebelement(xml.getlocator("//locators/Go2Date")),  df.format(new Date()));
			Thread.sleep(2000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2SolutionApproved")));
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2FinanceApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2FinanceApproved")));
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2LegalApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2LegalApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);

			//Go3 check
			WaitforElementtobeclickable(xml.getlocator("//locators/Go3check"));
			Clickon(getwebelement(xml.getlocator("//locators/Go3check")));
			Thread.sleep(2000);
			 
			WaitforElementtobeclickable(xml.getlocator("//locators/Go3Date"));
			SendKeys(getwebelement(xml.getlocator("//locators/Go3Date")),  df.format(new Date()));
			Thread.sleep(2000);
		}
		else
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/GovernanceTab"));
			Clickon(getwebelement(xml.getlocator("//locators/GovernanceTab")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0SolutionApproved")));
			
			if(isElementPresent(xml.getlocator("//locators/Go0Outcome")))
			{
			WaitforElementtobeclickable(xml.getlocator("//locators/Go0Outcome"));
			Clickon(getwebelement(xml.getlocator("//locators/Go0Outcome")));
			Thread.sleep(2000);
			WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes0"));
			Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes0")));
		    }
			try {
				if(isElementPresent(xml.getlocator("//locators/Go1Outcome")))
				{
				
				WaitforElementtobeclickable(xml.getlocator("//locators/Go1Outcome"));
				SendKeys(getwebelement(xml.getlocator("//locators/Go1Outcome")), "01 - Approved to proceed");
				Thread.sleep(2000);
				WaitforElementtobeclickable(xml.getlocator("//locators/Approvedoutcomes1"));
				Clickon(getwebelement(xml.getlocator("//locators/Approvedoutcomes1")));
				}}
				catch(Exception e)
				{
					System.out.println("Unable to find GO 1 OutComes");
				}
			Thread.sleep(2000);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1check"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1check")));
			Thread.sleep(2000);
			 DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
			
			
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1Date"));
			SendKeys(getwebelement(xml.getlocator("//locators/Go1Date")),  df.format(new Date()));
			Thread.sleep(2000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go1SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go1SolutionApproved")));
			
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SalesApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2SalesApproved")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/Go2SolutionApproved"));
			Clickon(getwebelement(xml.getlocator("//locators/Go2SolutionApproved")));
	
			
		}
		//Enter Sales Unit detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/ClickSaveOpty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/ClickSaveOpty")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
	}
	public void LoginAsC4CAddmin(String Application) throws Exception
	{
		
		openurl(Application);
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigated to "+Application+" Login Page");
		WaitforElementtobeclickable(xml4.getlocator("//locators/"+Application+"/Username"));  
		SendKeys(getwebelement(xml4.getlocator("//locators/"+Application+"/Username")),Getkeyvalue(Application+"_AdminUsername"));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter User Name");
		WaitforElementtobeclickable(xml.getlocator("//locators/"+Application+"/Password"));
		SendKeys(getwebelement(xml4.getlocator("//locators/"+Application+"/Password")),Getkeyvalue(Application+"_AdminPassword"));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Password");
		WaitforElementtobeclickable(xml4.getlocator("//locators/"+Application+"/Loginbutton"));
		
		
		Clickon(getwebelement(xml4.getlocator("//locators/"+Application+"/Loginbutton")));
		
		Thread.sleep(2000);
		
		for (int i = 1; i < 10; i++) {
			if (isElementPresent(xml.getlocator("//locators//LoginWarning"))){
				Clickon(getwebelement(xml.getlocator("//locators//Loginbutton")));
				Waittilljquesryupdated();
				Thread.sleep(3000);
				continue;
			} else {
				break;
			}
		} 
		
//		while(isElementPresent(xml4.getlocator("//locators//LoginWarning"))){
//			try {
//			Clickon(getwebelement(xml4.getlocator("//locators//Loginbutton")));
//			}
//			catch(Exception e) {
//				System.out.println("Continue the script");
//			}
//		}
		
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Login Button");
		
	}
	public void LoginAsC4CSales(String Application) throws Exception
	{
		openurl(Application);
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Navigated to "+Application+" Login Page");
		WaitforElementtobeclickable(xml4.getlocator("//locators/"+Application+"/Username"));  
		SendKeys(getwebelement(xml4.getlocator("//locators/"+Application+"/Username")),Getkeyvalue(Application+"_Username"));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter User Name");
		WaitforElementtobeclickable(xml.getlocator("//locators/"+Application+"/Password"));
		SendKeys(getwebelement(xml4.getlocator("//locators/"+Application+"/Password")),Getkeyvalue(Application+"_Password"));
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Password");
		WaitforElementtobeclickable(xml4.getlocator("//locators/"+Application+"/Loginbutton"));
		
		
		Clickon(getwebelement(xml4.getlocator("//locators/"+Application+"/Loginbutton")));
		
		Thread.sleep(2000);
//			try {
//			Clickon(getwebelement(xml.getlocator("//locators//Loginbutton")));
//			}
//			catch(Exception e) {
//				System.out.println("Conitnue the script");
//			}
//		}
		
		//ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Login Button");
		
	}
	public void Logout() throws Exception
	
	{
		WaitforElementtobeclickable(xml.getlocator("//locators/GOapproval/UserMenu"));
		Thread.sleep(1000);
		Clickon(getwebelement(xml.getlocator("//locators/GOapproval/UserMenu")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/GOapproval/Logoutlink"));
		Clickon(getwebelement(xml.getlocator("//locators/GOapproval/Logoutlink")));
	//	WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/GOapproval/Confirmation"));
		Thread.sleep(2000);
		Clickon(getwebelement(xml.getlocator("//locators/GOapproval/Confirmation")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/GOapproval/Relogin"));
		Clickon(getwebelement(xml.getlocator("//locators/GOapproval/Relogin")));
		
		
	}
	public void Opportunity_AddContainer(Object[][] Inputdata) throws Exception
	{
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/OpportunuityTab"));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/OpportunuityTab")));
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Tab in Account details");
		//Click on New to add Opportunity
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Newlink"));
		
		
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Newlink")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on New button to create New Opportunuity");
		//////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(10000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Name"));
		SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/Name")),"Auto_opty3");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		SendkeaboardKeys(getwebelement(xml.getlocator("//locators/Opportunity/Name")),Keys.ENTER);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Opportunuity Name");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		if(Inputdata[0][3].toString().equals("New Container")) 
		{
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Offer")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Offer"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "New Business")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Offer type");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		else if(Inputdata[0][3].toString().equals("Modify Container"))
		{
			Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Offer")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Offer"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Modification")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Offer type");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			
		}
		
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryProgramme"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryProgramme")),"Business Development");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryProgramme")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryProgramme"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Business Development")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Primary Program");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryAttribute"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryAttribute")),"Cisco");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/PrimaryAttribute")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/PrimaryAttribute"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Cisco")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter PrimaryAttribute");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency"))," ");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/OpportunityCurrency")),"US Dollar");
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OpportunityCurrency"));
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Singapore Dollar")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter Currency");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/More"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/More")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SaveOpenOpty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SaveOpenOpty")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save and Open Opportunuity Option");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Clickmore"));
		
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Clickmore")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on More link to Expand the Details");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(10000);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Editopty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Editopty")));
		
		//Enter Sales Unit detail
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		/*WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SalesUnit"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")));
		SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")),"Colt");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/SalesUnit"));
		//SendKeys(getwebelement(xml.getlocator("//locators/Opportunity/SalesUnit")),"Colt"); */
		//Enter Legal Complexity detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(10000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Edit Option to Edit the Details");
		
		
		
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Lcomplexity"));
		SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")),"");
		//////Thread.sleep(1000);
		//SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")),"Standard");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Lcomplexity")));
		//////Thread.sleep(3000);
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Standard")));
		//Enter Technical Complexity detail
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Legal Complexity");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/Tcomplexity"));
		//SendKeyswithAction(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")),"1");
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/Tcomplexity")));
		//////Thread.sleep(3000);
		Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "1")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(3000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Techincal Complexity");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/ClickSaveOpty"));
		Clickon(getwebelement(xml.getlocator("//locators/Opportunity/ClickSaveOpty")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Save Detail");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunity/OptyId"));
		OpportunityID.set(Gettext(getwebelement(xml.getlocator("//locators/Opportunity/OptyId"))));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Newly Created Opportunuity ID is"+OpportunityID.get().toString());
		}
	
	public void  MovetoOpportunuity(Object[][] Inputdata) throws Exception{
		System.out.println("Current Opp ID="+OpportunityID.get().toString());
		if((Inputdata[0][1].toString().equals("New"))&& OpportunityID.get().toString().equals("New"))
		{
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Opportunity_Add(Inputdata);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: New Opportunuty Has been created");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/oppo"));
		Thread.sleep(2000);
		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
		////////Thread.sleep(2000);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		else if(Inputdata[0][1].toString().equals("")&& !Inputdata[0][0].toString().equals("")){
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Price Lookup Quote without Opportunuity");
			//////Thread.sleep(2000);
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
			Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
			//////Thread.sleep(2000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		else if(Inputdata[0][1].toString().equals("")&& Inputdata[0][0].toString().equals("")){
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Price Lookup Quote without Opportunuity and Account");
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		else
		{
			if(OpportunityID.get().toString().equals("New")) {
			OpportunityID.set(Inputdata[0][1].toString());
			}
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Exiting Opportunuity ID is to be used"+OpportunityID.get().toString());
			
			WaitforElementtobeclickable(xml.getlocator("//locators/OpportunuityTab"));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/OpportunuityTab")));
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Tab");
		WaitforElementtobeclickable(xml.getlocator("//locators/AccountSearchField"));
		Search(OpportunityID.get().toString(),2);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Seach for Opportunuity");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunuitynumber").replace("Opportunuitynumber", OpportunityID.get().toString()));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/Opportunuitynumber").replace("Opportunuitynumber", OpportunityID.get().toString())));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Name ");
		////////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/oppo"));
		//////Thread.sleep(2000);
		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
		////////Thread.sleep(2000);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		System.out.println("Move Oppertunity finished off");
		
	}
	public void  VerifyQuoteStage() throws Exception {
		try {
			Clickon(getwebelement2(xml.getlocator("//locators/Proxylogout")));
		}
		catch(Exception e)
		{
			System.out.println("No Proxy Login enabled");
		}
		openurl2(CurrentQuoteURL.get().toString());
		//////Thread.sleep(15000);
		waitForpageload();
		getwebelement(xml2.getlocator("//locators/QuoteID"));
		Quotestatus.set(GetValueofInput(getwebelement(xml2.getlocator("//locators/Quotestatus"))));
		System.out.println("Quite Stage on Screee"+GetValueofInput(getwebelement(xml2.getlocator("//locators/Quotestatus"))));
		openurl("C4C");
		WaitforElementtobeclickable(xml.getlocator("//locators/SalesSection"));
		Clickon(getwebelement(xml.getlocator("//locators/SalesSection")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
		WaitforElementtobeclickable(xml.getlocator("//locators/Quotelink"));
		
		Clickon(getwebelement(xml.getlocator("//locators/Quotelink")));
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Search(QuoteID.get().toString(),1);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search for Current Quote ID"+QuoteID.get().toString());
		WaitforElementtobeclickable(xml.getlocator("//locators/Quotenumber").replace("Quotenumber", QuoteID.get().toString()));
		WaitforElementtobeclickable(xml.getlocator("//locators/C4CQuoteStage").replace("QuoteNumber", QuoteID.get().toString()));
		System.out.println("Quite Stage on Screeen"+GetText(getwebelement(xml.getlocator("//locators/C4CQuoteStage").replace("QuoteNumber", QuoteID.get().toString()))));
		SoftAssert Verify=new SoftAssert();
		Verify.assertTrue(GetText(getwebelement(xml.getlocator("//locators/C4CQuoteStage").replace("QuoteNumber", QuoteID.get().toString()))).equalsIgnoreCase(Quotestatus.get().toString()),"Quote Stage are matching");
		openurl2(CurrentQuoteURL.get().toString());
		//////Thread.sleep(15000);
		waitForpageload();
	}
	public void  MovetoOpportunuityContainer(Object[][] Inputdata) throws Exception{
		System.out.println("Current Opp ID="+OpportunityID.get().toString());
		if((Inputdata[0][1].toString().equals("New"))&& OpportunityID.get().toString().equals("New"))
		{
		Opportunity_AddContainer(Inputdata);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: New Opportunuty Has been created");
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/oppo"));
		//////Thread.sleep(2000);
		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
		////////Thread.sleep(2000);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		
		else
		{
			if(OpportunityID.get().toString().equals("New")) {
			OpportunityID.set(Inputdata[0][1].toString());
			}
			
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Exiting Opportunuity ID is to be used"+OpportunityID.get().toString());
			
			WaitforElementtobeclickable(xml.getlocator("//locators/OpportunuityTab"));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/OpportunuityTab")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Tab");
		
		WaitforElementtobeclickable(xml.getlocator("//locators/AccountSearchField"));
		System.out.println("Value"+OpportunityID.get().toString());
		Search(OpportunityID.get().toString(),2);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Seach for Opportunuity");
		WaitforElementtobeclickable(xml.getlocator("//locators/Opportunuitynumber").replace("Opportunuitynumber", OpportunityID.get().toString()));
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Clickon(getwebelement(xml.getlocator("//locators/Opportunuitynumber").replace("Opportunuitynumber", OpportunityID.get().toString())));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Opportunuity Name ");
		////////Thread.sleep(3000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		WaitforElementtobeclickable(xml.getlocator("//locators/oppo"));
		//////Thread.sleep(2000);
		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
		////////Thread.sleep(2000);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		
		
	}
	public void AddQuote() throws Exception {
		
		//while (gettitle().toString().contains("SAP Cloud for Customer")) {
//			System.out.println(gettitle().toString());
//			System.out.println(gettitle().toString().contains("SAP Cloud for Customer"));
//			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
//		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		////////Thread.sleep(5000);
		
		//////Thread.sleep(3000);
		WaitforElementtobeclickable(xml.getlocator("//locators/AddQuote"));
		//////Thread.sleep(3000);
		
		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
		//////Thread.sleep(10000);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			
		if(isElementPresent(xml.getlocator("//locators/PricelookupQuoteWithoutAccount"))) {	
			
		try {
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Add Colt Organization Country");
			WaitforElementtobeclickable(xml.getlocator("//locators/ColtOrganizationCountry"));
			Clickon(getwebelement(xml.getlocator("//locators/ColtOrganizationCountry")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "France")));
			//Enter Technical Complexity detail
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/SalesChanel"));
			Clickon(getwebelement(xml.getlocator("//locators/SalesChanel")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Wholesale")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		}
		catch(WebDriverException e)
		{
			System.out.println("Price Lookup in Account");
		}
		
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteCurrency"));
			Clickon(getwebelement(xml.getlocator("//locators/QuoteCurrency")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
//			Singapore Dollar
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Singapore Dollar")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupLegalComplexity")));
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Standard")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupTechnicalComplexity")));
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "1")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Techincal Complexity");
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupProceed")));
			
		}
		else if(isElementPresent(xml.getlocator("//locators/PricelookupQuote")))
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteCurrency"));
			Clickon(getwebelement(xml.getlocator("//locators/QuoteCurrency")));
			//////Thread.sleep(3000);
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Singapore Dollar")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupLegalComplexity")));
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "Standard")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupTechnicalComplexity")));
			//////Thread.sleep(3000);
			Clickon(getwebelement(xml.getlocator("//locators/optionlist").replace("option", "1")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Update Techincal Complexity");
			Clickon(getwebelement(xml.getlocator("//locators/PriceLookupProceed")));
		}
		else
		{
			System.out.println(" Nothing to update here");
		}
		
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Add Quote Button");
		System.out.print(gettitle().toString());
		//}
		System.out.println("Out of While loop");
	//	waitForpagenavigated(1);
		// Verify the Opportunuity and Account data
	}
	public void EditQuote() throws Exception {
		//////Thread.sleep(5000);
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//WaitforElementtobeclickable(xml.getlocator("//locators/AccountSearchField"));
		//Search(QuoteID.get().toString());
		WaitforElementtobeclickable(xml.getlocator("//locators/SalesSection"));
		Thread.sleep(2000);
		Clickon(getwebelement(xml.getlocator("//locators/SalesSection")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
		WaitforElementtobeclickable(xml.getlocator("//locators/Quotelink"));
		
		Clickon(getwebelement(xml.getlocator("//locators/Quotelink")));
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		Search(QuoteID.get().toString(),1);
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search for Current Quote ID"+QuoteID.get().toString());
		WaitforElementtobeclickable(xml.getlocator("//locators/Quotenumber").replace("Quotenumber", QuoteID.get().toString()));
		Clickon(getwebelement(xml.getlocator("//locators/Quotenumber").replace("Quotenumber", QuoteID.get().toString())));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clikc on Quote ID"+QuoteID.get().toString());
		
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		//////Thread.sleep(2000);
		//WaitforElementtobeclickable(xml.getlocator("//locators/OpenQuote"));
		
		if(isElementPresent(xml.getlocator("//locators/QuoteActions")))
		{	
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteActions"));
			Clickon(getwebelement(xml.getlocator("//locators/QuoteActions")));
		}
		else
		{
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteActions1"));
			Clickon(getwebelement(xml.getlocator("//locators/QuoteActions1")));
		}
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Actions Option");
		////////Thread.sleep(2000);
		WaitforElementtobeclickable(xml.getlocator("//locators/EditQuote"));
		Clickon(getwebelement(xml.getlocator("//locators/EditQuote")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Edit option");
		
		//WaitforElementtobeclickable(xml.getlocator("//locators/customersignatureTab"));
		
		waitForpagenavigated(1);
		waitForpageload();
		
	}
	
	public void Search(String Searchvalue,int i) throws Exception
	{
	
		//////Thread.sleep(2000);	
		System.out.println("Searching string is" +Searchvalue);
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			WaitforElementtobeclickable(xml.getlocator("//locators/AccountSearchField"));   
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Look for Search field");
			Thread.sleep(2000);
			Clickon(getwebelement(xml.getlocator("//locators/AccountSearchField")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter search value & hit Enter");
			System.out.println("("+xml.getlocator("//locators/SearchValue")+")["+i+"]");
			SendKeys(getwebelement("("+xml.getlocator("//locators/SearchValue")+")["+i+"]"), Searchvalue);
			SendkeaboardKeys(getwebelement("("+xml.getlocator("//locators/SearchValue")+")["+i+"]"),Keys.ENTER);
			////////Thread.sleep(3000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Enter the Search keyword-"+Searchvalue);
			
			
	}
	public void CheckdocumentSigned() throws Exception
	{
		////////Thread.sleep(30000);
		Pagerefresh();
		waitForpageload();
		Thread.sleep(30000);
		WaitforElementtobeclickable(xml.getlocator("//locators/customersignatureTab"));
		Clickon(getwebelement(xml.getlocator("//locators/customersignatureTab")));
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Customer Signature Tab");
		
		waitForpageload();
	if(!(Gettext(getwebelement(xml.getlocator("//locators/Status"))).equals("Signed")))
	{
//		WaitforElementtobeclickable(xml.getlocator("//locators/Refresh"));
//		Clickon(getwebelement(xml.getlocator("//locators/Refresh")));
		Pagerefresh();
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Refresh link");
		
	}
	}
public void AddContainerQuote(Object[][] data) throws Exception {
		
		//while (gettitle().toString().contains("SAP Cloud for Customer")) {
//			System.out.println(gettitle().toString());
//			System.out.println(gettitle().toString().contains("SAP Cloud for Customer"));
//			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
//		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
		////////Thread.sleep(5000);
	WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
	WaitforElementtobeclickable(xml.getlocator("//locators/oppo"));
	//////Thread.sleep(2000);
	WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
	Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
	
		WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
		
		if(data[0][3].toString().equals("New Container")) 
		{
			WaitforElementtobeclickable(xml3.getlocator("//locators/ContainerNewBusiness"));
			Clickon(getwebelement(xml3.getlocator("//locators/ContainerNewBusiness")));
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
		}
		else if(data[0][3].toString().equals("Modify Container"))
		{
			WaitforElementtobeclickable(xml3.getlocator("//locators/ContainerModify"));
			Clickon(getwebelement(xml3.getlocator("//locators/ContainerModify")));
			WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
			
		}
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Add Quote Button");
		System.out.print(gettitle().toString());
		//}
		System.out.println("Out of While loop");
		waitForpagenavigated(1);
//		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
//		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
//		//////Thread.sleep(5000);
//		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/QuoteTab"));
//		Clickon(getwebelement(xml.getlocator("//locators/QuoteTab")));
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Tab in Opportunuity Detail page ");
//		//////Thread.sleep(5000);
//		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
//		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/AddQuote"));
//		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
//		WaitforElementtobeclickable(xml.getlocator("//locators/AddQuote"));
//		Clickon(getwebelement(xml.getlocator("//locators/AddQuote")));
//		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Add to Quote Button");
		//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
	}
public void Submitformlogin() throws InterruptedException
{
	EnterText2(Keys.ENTER);
}
public void proxylogininCPQ() throws DocumentException, InterruptedException, Exception
{
	ProxyLogin("CPQ_Testing_User",xml2.getlocator("//locators/ProxyLink"));
}

//public boolean isCopyQuote(Object[][] data)
//{
//	boolean flag=false;
//	 for(int i=0;i<data.length;i++) {
//
//	  if(!data[i][65].toString().equals(""))
//	  {
//		  ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Copy Quote from Exiting Configuration!!");
//		  flag=true;
//	  }
//	  else {
//		  flag= false;
//	  }	
//	 
//	 }	
//			
//	 return flag;
//	
//}

public void CopyQuote(String Quotetype, Object[][] Inputdata) throws Exception
{
	switch(Quotetype)
	{
	  
	   case "C4C" :
	  { 
	     openurl("C4C");
	     WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
	     WaitforElementtobeclickable(xml.getlocator("//locators/SalesSection"));
	        Thread.sleep(2000);
			Clickon(getwebelement(xml.getlocator("//locators/SalesSection")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on the Customer Section in Left Navigation");
			WaitforElementtobeclickable(xml.getlocator("//locators/Quotelink"));
			
			Clickon(getwebelement(xml.getlocator("//locators/Quotelink")));
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			Search(QuoteID.get().toString(),1);
			
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Search for Current Quote ID"+QuoteID.get().toString());
			WaitforElementtobeclickable(xml.getlocator("//locators/Quotenumber").replace("Quotenumber", QuoteID.get().toString()));
			Clickon(getwebelement(xml.getlocator("//locators/Quotenumber").replace("Quotenumber", QuoteID.get().toString())));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clikc on Quote ID"+QuoteID.get().toString());
			
			WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			//////Thread.sleep(2000);
			//WaitforElementtobeclickable(xml.getlocator("//locators/OpenQuote"));
			
			if(isElementPresent(xml.getlocator("//locators/QuoteActions")))
			{	
				WaitforElementtobeclickable(xml.getlocator("//locators/QuoteActions"));
				Clickon(getwebelement(xml.getlocator("//locators/QuoteActions")));
			}
			else
			{
				WaitforElementtobeclickable(xml.getlocator("//locators/QuoteActions1"));
				Clickon(getwebelement(xml.getlocator("//locators/QuoteActions1")));
			}
			//WaitforC4Cloader(xml.getlocator("//locators/C4Cloader"),1);
			
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Actions Option");
	  
			WaitforElementtobeclickable(xml.getlocator("//locators/CopyQuote"));
			Clickon(getwebelement(xml.getlocator("//locators/CopyQuote")));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Click on Quote Copy option");	
//		    WaitforElementtobeclickable(xml.getlocator("//locators/OppertunityIDforcopyQuote"));
//		    Clear(getwebelement(xml.getlocator("//locators/OppertunityIDforcopyQuote")));
//		    SendKeys(getwebelement(xml.getlocator("//locators/OppertunityIDforcopyQuote")), data[i][65].toString());
		    
			WaitforElementtobeclickable(xml.getlocator("//locators/ProceedBtn"));
			Clickon(getwebelement(xml.getlocator("//locators/ProceedBtn")));
			waitForpagenavigated(1);
			waitForpageload();
			break;
	  
	  }
	   case "Line Item" :
	   { 
		   for(int i=0;i<Inputdata.length;i++)
		   {
			   WaitforElementtobeclickable(xml.getlocator("//locators/ProductSelector").replace("index", String.valueOf(i+1)));
			   Clickon(getwebelement(xml.getlocator("//locators/ProductSelector").replace("index", String.valueOf(i+1)))); 
		   }
		    WaitforElementtobeclickable(xml.getlocator("//locators/CopyQuoteFromLineItem"));
			Clickon(getwebelement(xml.getlocator("//locators/CopyQuoteFromLineItem"))); 
			waitForpagenavigated(1);
			waitForpageload();
		   break;
	   }   
		   
	   case "Transaction" :
	   { 
		    WaitforElementtobeclickable(xml.getlocator("//locators/Admin"));
			Clickon(getwebelement(xml.getlocator("//locators/Admin"))); 
			//waitForpageload();
			Thread.sleep(4000);
			WaitforElementtobeclickable(xml.getlocator("//locators/TransactionLink"));
			Clickon(getwebelement(xml.getlocator("//locators/TransactionLink")));
			WaitforElementtobeclickable(xml.getlocator("//locators/SelectQuoteforCopy").replace("QuoteNumber", QuoteID.get().toString()));
			Clickon(getwebelement(xml.getlocator("//locators/SelectQuoteforCopy").replace("QuoteNumber", QuoteID.get().toString())));
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clikc on Quote ID"+QuoteID.get().toString());
			WaitforElementtobeclickable(xml.getlocator("//locators/CopyQuotefromTransaction"));
			Clickon(getwebelement(xml.getlocator("//locators/CopyQuotefromTransaction"))); 
			waitForpagenavigated(1);
			waitForpageload();
			break;
			
	   }   
	  default :
	  {
		  System.out.println("Copy Quote with this option is not available in CPQ Application");
		  break;
	  }
	}
//	  if(isElementPresent(xml.getlocator("//locators/okbtnforpriceupdate")))
//	  {
//		  Clickon(getwebelement(xml.getlocator("//locators/okbtnforpriceupdate")));  
//		  waitForpageload();
//		  Thread.sleep(3000);
//	  }
	  Pagerefresh();
	  waitForpageload();
	  Thread.sleep(2000);
	  QuoteID.set(GetValueofInput(getwebelement(xml.getlocator("//locators/QuoteID"))));
	 System.out.println(QuoteID.get());
	 ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Clikc on Quote ID"+QuoteID.get().toString());
	   
}   
		   
	  public void CopyQuoteItem(Object[][] data) throws Exception
	  {
	  	
//	  	 for(int i=0;i<data.length;i++) {
		  String Quotetype = data[0][64].toString();	
	  	  CopyQuote(Quotetype,data);
		   
		   }


//}
 
}
