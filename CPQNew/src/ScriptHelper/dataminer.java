package ScriptHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class dataminer {
	
	@SuppressWarnings("deprecation")
	public static String fngetcolvalue(String file_name,String sheet_name, String iScript, String iSubScript, String col_name )throws IOException
	/*----------------------------------------------------------------------
	Method Name : fngetcolvalue
	Purpose     : to get the column value of the required column name
	Designer    : Vasantharaja C
	Created on  : 1st April 2020
	Input       : file_name,sheet_name, iScript, iSubScript, col_name
	Output      : sVal3
	 ----------------------------------------------------------------------*/	
	{
		FileInputStream file_stream = new FileInputStream(file_name);
		@SuppressWarnings("resource")
		XSSFWorkbook work_book = new XSSFWorkbook(file_stream);
		XSSFSheet work_sheet = work_book.getSheet(sheet_name);
		//int row_count = work_sheet.getlastCellNum();
		XSSFRow row = work_sheet.getRow(0);
		int last_row = work_sheet.getLastRowNum();
		int last_Col = row.getLastCellNum();
		//XSSFCell cell_val =  work_sheet.getRow(0).getCell(1);
		int isScript = 0,iter = 0,isSubScript = 0,act_row = 0,act_col = 0;
		String sVal3 = "";
		for (iter = 0; iter<=last_Col-1; iter++)
		{
			if (row.getCell(iter).getStringCellValue().trim().equals("iScript"))
				isScript = iter;
			if (row.getCell(iter).getStringCellValue().trim().equals("iSubScript"))
				isSubScript = iter;
			if (row.getCell(iter).getStringCellValue().trim().equals(col_name))
				act_col = iter;
		}
		
		for (int iRow = 1; iRow <= last_row; iRow++)
		   {
			   row = work_sheet.getRow(iRow);
			   Cell cell = row.getCell(isScript);
			   cell.setCellType(CellType.STRING);
			   String sVal1 = row.getCell(isScript).getStringCellValue();
			   String sVal2 = row.getCell(isSubScript).getStringCellValue();
			   if (sVal1.equalsIgnoreCase(iScript) && sVal2.equalsIgnoreCase(iSubScript))
			   {
				   act_row = iRow;
				   //System.out.println("actual row value "+act_row);
				   row = work_sheet.getRow(act_row);
				   try {
					   sVal3 = row.getCell(act_col).getStringCellValue();
				   } catch (Exception e) {
						return "";
					}
			   }
		   }
		  return sVal3;
		
	   //work_book.close();
	   //file_stream.close();
	}
	
	public static void fnsetcolvalue(String file_name,String sheet_name, String iScript, String iSubScript, String col_name, String col_value )throws IOException
	/*----------------------------------------------------------------------
	Method Name : fnsetcolvalue
	Purpose     : to enter the value to required cell
	Designer    : Vasantharaja C
	Created on  : 1st April 2020
	Input       : file_name,sheet_name, iScript, iSubScript, col_name, col_value
	Output      : sVal3
	 ----------------------------------------------------------------------*/	
	{
			FileInputStream file_stream = new FileInputStream(file_name);
			@SuppressWarnings("resource")
			XSSFWorkbook work_book = new XSSFWorkbook(file_stream);
			XSSFSheet work_sheet = work_book.getSheet(sheet_name);
			//int row_count = work_sheet.getlastCellNum();
			XSSFRow row = work_sheet.getRow(0);
			int last_row = work_sheet.getLastRowNum();
			int last_Col = row.getLastCellNum();
			//XSSFCell cell_val =  work_sheet.getRow(0).getCell(1);
			int isScript = 0,iter = 0,isSubScript = 0,act_row = 0,act_col = 0;
			for (iter = 0; iter<=last_Col-1; iter++)
			{
				if (row.getCell(iter).getStringCellValue().trim().equals("iScript"))
					isScript = iter;
				if (row.getCell(iter).getStringCellValue().trim().equals("iSubScript"))
					isSubScript = iter;
				if (row.getCell(iter).getStringCellValue().trim().equals(col_name))
					act_col = iter;
			}
		   for (int iRow = 1; iRow <= last_row; iRow++)
		   {
			   row = work_sheet.getRow(iRow);
			   Cell cell = row.getCell(isScript);
			   cell.setCellType(CellType.STRING);
			   String sVal1 = row.getCell(isScript).getStringCellValue();
			   String sVal2 = row.getCell(isSubScript).getStringCellValue();
			   if (sVal1.equalsIgnoreCase(iScript) && sVal2.equalsIgnoreCase(iSubScript))
			   {
				   act_row = iRow;
				   //System.out.println("actual row value "+act_row);
				   row = work_sheet.getRow(act_row);
				   cell = row.getCell(act_col);
				   cell.setCellType(CellType.STRING);
				   cell.setCellValue(col_value);
				   //row.getCell(act_col).setCellValue(col_value);
				   file_stream.close();
				   FileOutputStream fos = new FileOutputStream(file_name);
				   work_book.write(fos);
				   fos.close();
			   }
		   }
	}
	
	public static int fngetrowcount(String file_name,String sheet_name ) throws IOException
	/*----------------------------------------------------------------------
	Method Name : fngetrowcount
	Purpose     : to get used row count of a particular excel sheet
	Designer    : Vasantharaja C
	Created on  : 1st April 2020
	Input       : file_name,sheet_name
	Output      : last_row
	 ----------------------------------------------------------------------*/	
	{
		FileInputStream file_stream = new FileInputStream(file_name);
		@SuppressWarnings("resource")
		XSSFWorkbook work_book = new XSSFWorkbook(file_stream);
		XSSFSheet work_sheet = work_book.getSheet(sheet_name);
		int last_row = work_sheet.getLastRowNum();
		return last_row;
	}
	
	public static String fngetScriptval(String file_name,String sheet_name, int iRow, String rSript)throws IOException
	/*----------------------------------------------------------------------
	Method Name : fngetScriptval
	Purpose     : to get column Value of iScript on a current iteration
	Designer    : Vasantharaja C
	Created on  : 1st April 2020
	Input       : file_name,sheet_name, iRow, rSript
	Output      : sVal
	 ----------------------------------------------------------------------*/	
	{
		FileInputStream file_stream = new FileInputStream(file_name);
		@SuppressWarnings("resource")
		XSSFWorkbook work_book = new XSSFWorkbook(file_stream);
		XSSFSheet work_sheet = work_book.getSheet(sheet_name);
		XSSFRow row = work_sheet.getRow(0);
		int last_Col = row.getLastCellNum();
		int isScript = 0,iter = 0; String sVal = null;
		for (iter = 0; iter<=last_Col-1; iter++)
		{
			if (row.getCell(iter).getStringCellValue().trim().equals(rSript))
				isScript = iter;
		}
		   row = work_sheet.getRow(iRow);
		   try {
			   sVal = row.getCell(isScript).getStringCellValue();
		   } catch (Exception e) {
			   sVal = "";
		   }
		return sVal;
	}
	
	public static void writeexcel(String[] data )throws IOException
	
	{
		 FileInputStream file = new FileInputStream(new File("src\\Data\\ServiceOrder\\Service.xlsx"));
			@SuppressWarnings("resource")
			XSSFWorkbook work_book = new XSSFWorkbook(file);
			XSSFSheet work_sheet = work_book.getSheetAt(0);
			int rowCount = work_sheet.getLastRowNum()-work_sheet.getFirstRowNum();
			Row row = work_sheet.getRow(0);
			 Row newRow = work_sheet.createRow(rowCount+1);
			 for(int j = 0; j < row.getLastCellNum(); j++){

			        //Fill data in row

			        Cell cell = newRow.createCell(j);

			        cell.setCellValue(data[j]);

			    }
			 file.close();
			 FileOutputStream fos = new FileOutputStream("src\\Data\\ServiceOrder\\Service.xlsx");
			   work_book.write(fos);
			   fos.close();
	}

}
