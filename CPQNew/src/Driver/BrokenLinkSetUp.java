package Driver;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

import Reporter.ExtentTestManager;



public class BrokenLinkSetUp  {

	// All Link
	
			@FindBy(tagName="a")
			private List<WebElement> alllink;
			
			public BrokenLinkSetUp(WebDriver driver)
			{
				PageFactory.initElements(driver, this);
			}
					
			
			public  void verifyLink(String urlLink) {
		        //Sometimes we may face exception "java.net.MalformedURLException". Keep the code in try catch block to continue the broken link analysis
		        try {
		            //Use URL Class - Create object of the URL Class and pass the urlLink as parameter 
		            URL link = new URL(urlLink);
		            // Create a connection using URL object (i.e., link)
		            HttpURLConnection httpConn =(HttpURLConnection)link.openConnection();
		            //Set the timeout for 2 seconds
		            httpConn.setConnectTimeout(2000);
		            //connect using connect method
		            httpConn.connect();
		            //use getResponseCode() to get the response code. 
		                if(httpConn.getResponseCode()== 200) {  
		                    //log(urlLink+" - "+httpConn.getResponseMessage());
		                    Assert.assertTrue(httpConn.getResponseMessage().contains("OK"),"<font color='red'>Some issue with URL --- "+ urlLink + "</font>");
		                	ExtentTestManager.getTest().log(LogStatus.PASS, "<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");  
		                    Reporter.log("<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");
		                }
		                if(httpConn.getResponseCode()== 404) {
		                   //log(urlLink+" - "+httpConn.getResponseMessage());
		                   Assert.assertTrue(httpConn.getResponseMessage().contains("OK"),"<font color='red'>Some issue with URL --- "+ urlLink + "</font>");
		                   ExtentTestManager.getTest().log(LogStatus.PASS, "<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");  
		                    Reporter.log("<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");

		                }
		                                if(httpConn.getResponseCode()== 400) { 
		                                      // log(urlLink+" - "+httpConn.getResponseMessage()); 
		                                        Assert.assertTrue(httpConn.getResponseMessage().contains("OK"),"<font color='red'>Some issue with URL ---"+ urlLink + "</font>");
		                                        ExtentTestManager.getTest().log(LogStatus.PASS, "<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");  
		            		                    Reporter.log("<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");

		                                }
		                                
		                                if(httpConn.getResponseCode()== 500) {
		                    //log(urlLink+" - "+httpConn.getResponseMessage()); 
		                    Assert.assertTrue(httpConn.getResponseMessage().contains("OK"),"<font color='red'>Some issue with URL --- "+ urlLink + "</font>");
		                    ExtentTestManager.getTest().log(LogStatus.PASS, "<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");  
		                    Reporter.log("<font color='green'>Link "+urlLink +" is working Properly with HTTP Response Code message ---- " +httpConn.getResponseMessage() + " </font>");
        
		                                }
		                                
		                             }
		            //getResponseCode method returns = IOException - if an error occurred connecting to the server. 
		        catch (Exception e) {
		            //e.printStackTrace();
		         
		        }
		    } 	
			
			
			public void VerifyBrokenLink(WebDriver driver)
			{
				int TotalLink = alllink.size();
                ExtentTestManager.getTest().log(LogStatus.PASS, "Total Link is "+TotalLink);  
                Reporter.log("Total Link is "+TotalLink );
	
				 for(int i=0; i<TotalLink; i++) {
			            WebElement element = alllink.get(i);
			            String url=element.getAttribute("href");
			            if(url==null)
			            {
			            	 ExtentTestManager.getTest().log(LogStatus.PASS, "Link does not have Href Attribute");  	
			            }
			            else {
			            verifyLink(url);  
			            }
			        }   
				
			}
			
		
	
}