package Driver;

import java.util.Locale;

import javax.sound.sampled.AudioSystem;
import javax.speech.Central;
import javax.speech.EngineException;
import javax.speech.synthesis.Synthesizer; 
import javax.speech.synthesis.SynthesizerModeDesc;
import javax.speech.synthesis.Voice;


  

public class TextSpeech {

//	 public static void main(String[] args) 
//	    { 
//	  
//	        try { 
//	            // Set property as Kevin Dictionary 
//	        	  System.setProperty( 
//		  	                "freetts.voices", 
//		  	                "com.sun.speech.freetts.en.us"
//		  	                    + ".cmu_us_kal.KevinVoiceDirectory"); 
////	  
//	            // Register Engine 
//	            Central.registerEngineCentral( 
//	                "com.sun.speech.freetts"
//	                + ".jsapi.FreeTTSEngineCentral"); 
//	            //System.setProperty("mbrola.base", "C:\\Users\\iup\\workspace\\newpro\\mbrola");
//	  
//	            // Create a Synthesizer 
//	            Synthesizer synthesizer 
//	                = Central.createSynthesizer( 
//	                    new SynthesizerModeDesc(Locale.US)); 
//	  
//	            // Allocate synthesizer 
//	            synthesizer.allocate(); 
//	  
//	            // Resume Synthesizer 
//	            synthesizer.resume(); 
//	  
//	            // Speaks the given text 
//	            // until the queue is empty. 
//	            synthesizer.speakPlainText( 
//	                "GeeksforGeeks", null); 
//	            synthesizer.waitEngineState( 
//	                Synthesizer.QUEUE_EMPTY); 
//	  
//	            // Deallocate the Synthesizer. 
//	            synthesizer.deallocate(); 
//	        } 
//	  
//	        catch (Exception e) { 
//	            e.printStackTrace(); 
//	        } 
//	    } 
//	
//}
//		    String speaktext;
//
//		    public void dospeak(String speak,String  voicename)
//		    {
//		        speaktext=speak;
//		        String voiceName =voicename;
//		        try
//		        {
//		        	  System.setProperty( 
//		  	                "freetts.voices", 
//		  	                "com.sun.speech.freetts.en.us"
//		  	                    + ".cmu_us_kal.KevinVoiceDirectory"); 
//		            SynthesizerModeDesc desc = new SynthesizerModeDesc(null,"general",  Locale.US,null,null);
//		            Synthesizer synthesizer =  Central.createSynthesizer(desc);
//		            synthesizer.allocate();
//		            synthesizer.resume();
//		            desc = (SynthesizerModeDesc)  synthesizer.getEngineModeDesc();
//		            Voice[] voices = desc.getVoices();
//		            Voice voice = null;
//		            for (int i = 0; i < voices.length; i++)
//		            {
//		                if (voices[i].getName().equals(voiceName))
//		                {
//		                    voice = voices[i];
//		                    break;
//		                }
//		            }
//		            synthesizer.getSynthesizerProperties().setVoice(voice);
//		            System.out.print("Speaking : "+speaktext);
//		            synthesizer.speakPlainText(speaktext, null);
//		            synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);
//		            synthesizer.deallocate();
//		        }
//		        catch (Exception e)
//		        {
//		            String message = " missing speech.properties in " + System.getProperty("user.home") + "\n";
//		            System.out.println(""+e);
//		            System.out.println(message);
//		        }
//		    }


	
	
	
	public static void speech(String text) throws EngineException {
	    if (text == null || text.trim().isEmpty()) return;

	   
	    String voiceName = "kevin16";
       
	    try {
	    	
	       
	    	 
	 	    System.setProperty("FreeTTSSynthEngineCentral", "com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
	         System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
	         Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");
	         SynthesizerModeDesc desc = new SynthesizerModeDesc(null, "general", Locale.US, null, null);
	         Synthesizer synth = Central.createSynthesizer(desc);
	      
	
	        
	      
	        synth.allocate();
	        
	        desc = (SynthesizerModeDesc) synth.getEngineModeDesc();
	        Voice[] voices = desc.getVoices();
	        Voice voice = null;
	     
	        for (Voice entry : voices) {
	            if(entry.getName().equals(voiceName)) {
	                voice = entry;
	                break;
	            }
	        }
	        synth.getSynthesizerProperties().setVoice(voice);
	        //synth.resume();
	       // synth.
	        synth.speakPlainText(text, null);
//	        synth.synthesize
//	       // synth.
//	        if (!AudioSystem.au) {
//                LOGGER.log(Level.SEVERE,"Line not supported");
//            }
	        //synth.waitEngineState(Synthesizer.);
	       //synth.waitEngineState(Synthesizer.DEALLOCATED);
	        //synth.pause();
	        synth.waitEngineState(Synthesizer.QUEUE_EMPTY);
	        
	   //     synth.deallocate();
	       // synth.waitEngineState(Synthesizer.DEALLOCATED);
	       
	        

	    } catch(Exception ex) {
	        String message = " missing speech.properties in " + System.getProperty("user.home") + "\n";
	        System.out.println("" + ex);
	        System.out.println(message);
	        ex.printStackTrace();
	    }
	    
}
	

    public static void main(String[] args) throws EngineException
    {
    	//TextSpeech TX = new TextSpeech();
//    	TX.dospeak("abc", "tEXT NAME");
    	
    	TextSpeech.speech("My Name Is Khan");
    
    	TextSpeech.speech("My Name Is Amir");
    }


}




