package Driver;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.varia.ReloadingPropertyConfigurator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.beust.jcommander.Parameter;

public class PropertyReader  {

//  //<!-- https://mvnrepository.com/artifact/org.apache.pdfbox/pdfbox-app -->
//<dependency>
//    <groupId>org.apache.pdfbox</groupId>
//    <artifactId>pdfbox-app</artifactId>
//    <version>2.0.11</version>
//</dependency>
//<dependency>
//    <groupId>org.apache.pdfbox</groupId>
//    <artifactId>pdfbox-app</artifactId>
//    <version>1.8.15</version>
//</dependency>
	
	
	public String readproperty(String key,String environment) throws IOException
	{ 
		InputStream file;
    	String St=null;
//    	DriverTestcase dri =  new DriverTestcase();	
	Properties pr= new Properties();
	System.out.println("Test methods are");
	
	
	if(environment.equalsIgnoreCase("UAT2"))
	{
		System.out.println("1");
		 file= new FileInputStream("src\\Config\\configUAT2.properties");
	}
	else if(environment.equalsIgnoreCase("Test1"))
	{
		System.out.println("2");
		file= new FileInputStream("src\\Config\\configTest1.properties");
	}
	else if(environment.equalsIgnoreCase("RFS"))
	{	System.out.println("3");
	file= new FileInputStream("src\\Config\\configRFS.properties");
	}
	else if(environment.equalsIgnoreCase("Prod"))
	{	System.out.println("3");
	file= new FileInputStream("src\\Config\\configProd.properties");
	}
	else if(environment.equalsIgnoreCase("Test2"))
	{	System.out.println("3");
	file= new FileInputStream("src\\Config\\configTest2.properties");
	}
	
	else {
		System.out.println("4");
		file= new FileInputStream("src\\Config\\config.properties");
	}
	pr.load(file);
	St=pr.getProperty(key);
	file.close();
	return St;
		
	}
	
	public String readsessionproperty(String key) throws IOException
	{ String St=null;
	
	Properties pr= new Properties();
	InputStream file= new FileInputStream("src\\Config\\Debug.properties");
	pr.load(file);
	St=pr.getProperty(key);
	file.close();
	return St;
		
	}
	
	public void updateproprty(String key,String value) throws IOException {
		FileInputStream in = new FileInputStream("src\\Config\\Debug.properties");
		Properties props = new Properties();
		props.load(in);
		in.close();

		FileOutputStream out = new FileOutputStream("src\\Config\\Debug.properties");
		props.setProperty(key, value);
		props.store(out, null);
		out.close(); 
	}

	public static void main(String args[]) throws IOException
	{
		PropertyReader pr=new PropertyReader();
		pr.updateproprty("SessionID","abcd");
	}
	
}
