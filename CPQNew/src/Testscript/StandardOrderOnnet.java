package Testscript;
import java.io.IOException;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import Driver.BrokenLinkSetUp;
import Driver.DataReader;
import Driver.DriverTestcase;
import Reporter.ExtentTestManager;
import ScriptHelper.BespokandNonStandard;

public class StandardOrderOnnet extends DriverTestcase {	
	
@Test(dataProviderClass=DataReader.class,dataProvider="NewStandrdOrder")
	public void EndtoEndOrder(Object[][] Data) throws Exception
	{
		//ExtentTestManager.getTest().setDescription("Login Into C4C");
		//Login.get().Login1("CPQ");
		//C4Chelper.get().Submitformlogin();
		//C4Chelper.get().proxylogininCPQ();
		
	     Login.get().Login("C4C"); 
	   //C4Chelper.get().proxylogininCPQ();
	     
	
	     if(Data[0][79].toString().equalsIgnoreCase("Yes")) {
	    	 Login.get().LaunchUrlAgain("C4C");
	    	 C4Chelper.get().Waittilljquesryupdated();
	     }
		
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Move to Account");
		C4Chelper.get().Movetoaccount(Data);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Move to Opportunity");
		C4Chelper.get().MovetoOpportunuity(Data);
		
		Thread.sleep(3000);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Add Quote");
		C4Chelper.get().AddQuote();
		if(Data[0][73].toString().equalsIgnoreCase("Yes"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Select the Multi-Quote as Quote stage to proceed further!!!");
			Configurationhelper.get().MultiQuoteselect(Data);
		}
		
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Add Product");
		Configurationhelper.get().AddProduct(Data);
		if(Data[0][73].toString().equalsIgnoreCase("Yes"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Proceed to convert Multi Quote to Standard!");
			Configurationhelper.get().MultiQuoteProcess(Data);
		}
	
		// This Condition in only executed for Professional Service proces
		if(Configurationhelper.get().Quotestatus.get().equals("Created"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method ProfessionalServiceReconfigure");
			Configurationhelper.get().ProfessionalServiceReconfigure(Data);
	
			//C4Chelper.get().VerifyQuoteStage();
		}
		
		if(Configurationhelper.get().Quotestatus.get().equals("Created"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method SEDataUpdate");
			Configurationhelper.get().SEDataupdate(Data);
	
			//C4Chelper.get().VerifyQuoteStage();
		}
		//C4Chelper.get().VerifyQuoteStage();
		if(Configurationhelper.get().Quotestatus.get().equals("Waiting for BCP"))
		{
			System.out.println("inBCP");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to Explore");
			Explorehelper.get().NavigatetoExplore();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Logout Explore");
			Login.get().Logout("Explore");
			//Thread.sleep(30000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Login Explore Nearnet");
			Login.get().Login("ExploreNearNet");
			if(Data[0][79].toString().equalsIgnoreCase("Yes")) {
		    	 Login.get().LaunchUrlAgain("ExploreNearNet");
		     }
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Explore Workflow Nearnet");
			Explorehelper.get().ExploreWorkflownearnet(Data);
			Login.get().Logout("ExploreNearNet");
			//Thread.sleep(30000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Login C4C");
			Login.get().Login("C4C");
			if(Data[0][79].toString().equalsIgnoreCase("Yes")) {
		    	 Login.get().LaunchUrlAgain("C4C");
		     }
			
			//C4Chelper.get().Movetoaccount(Data);
			//C4Chelper.get().MovetoOpportunuity(Data);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Edit Quote");
			C4Chelper.get().EditQuote();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure");
			Configurationhelper.get().Reconfigure(Data);
			//C4Chelper.get().VerifyQuoteStage();
//			System.out.println("Reoccurane Value currently as:"+Configurationhelper.get().Rerunrequired.get().toString());
			if(Configurationhelper.get().Rerunrequired.get().equals("Yes"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure Again");	
				Configurationhelper.get().ReconfigureAgain(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to Explore");
				Explorehelper.get().NavigatetoExplore();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Logout Explore");
				Login.get().Logout("Explore");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Login Explore Nearnet");
				Login.get().Login("ExploreNearNet");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Explore Workflow nearnet");
				Explorehelper.get().ExploreWorkflownearnet(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Logout Explore Nearnet");
				Login.get().Logout("ExploreNearNet");
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Login C4C");
				Login.get().Login("C4C");
				
				//C4Chelper.get().Movetoaccount(Data);
				//C4Chelper.get().MovetoOpportunuity(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Edit Quote");
				C4Chelper.get().EditQuote();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure");
				Configurationhelper.get().Reconfigure(Data);
				//C4Chelper.get().VerifyQuoteStage();
			}
	    }
		if(Configurationhelper.get().Quotestatus.get().equals("Waiting for 3rd Party"))
		{
			System.out.println("in 3rd party");
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to Explore");
			Explorehelper.get().NavigatetoExplore();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Logout Explore");
			Login.get().Logout("Explore");
			//Thread.sleep(30000);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Login Explore");
			Login.get().Login("Explore");
			if(Data[0][79].toString().equalsIgnoreCase("Yes")) {
		    	 Login.get().LaunchUrlAgain("Explore");
		     }
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Explore Workflow");
		//	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to Explore");
		//	Explorehelper.get().NavigatetoExplore();
		//	ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Explore Workflow");
			Explorehelper.get().ExploreWorkflow(Data);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to C4C");
			C4Chelper.get().NavigatetoC4C();
			C4Chelper.get().Logout();
			C4Chelper.get().LoginAsC4CSales("C4C");
			if(Data[0][79].toString().equalsIgnoreCase("Yes")) {
		    	 Login.get().LaunchUrlAgain("C4C");
		     }
			//C4Chelper.get().Movetoaccount(Data);
			//C4Chelper.get().MovetoOpportunuity(Data);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Edit Quote");
			C4Chelper.get().EditQuote();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure");
			Configurationhelper.get().Reconfigure(Data);
			//C4Chelper.get().VerifyQuoteStage();
//			System.out.println("Reoccurane Value currently as:"+Configurationhelper.get().Rerunrequired.get().toString());
			if(Configurationhelper.get().Rerunrequired.get().equals("Yes"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure Again");	
				Configurationhelper.get().ReconfigureAgain(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to Explore");
				Explorehelper.get().NavigatetoExplore();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Explore Workflow");
				Explorehelper.get().ExploreWorkflow(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate to C4C");
				C4Chelper.get().NavigatetoC4C();
				//C4Chelper.get().Movetoaccount(Data);
				//C4Chelper.get().MovetoOpportunuity(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Edit Quote");
				C4Chelper.get().EditQuote();
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Reconfigure");
				Configurationhelper.get().Reconfigure(Data);
				//C4Chelper.get().VerifyQuoteStage();
			}
		}
		if(Configurationhelper.get().Quotestatus.get().equals("POA"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method POA");
			Configurationhelper.get().POA();
			//C4Chelper.get().VerifyQuoteStage();
		}
		if(Configurationhelper.get().Quotestatus.get().equals("SE Configuration"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method SEDataUpdate");
			Configurationhelper.get().SEDataupdate(Data);
	
			//C4Chelper.get().VerifyQuoteStage();
		}
		if(Configurationhelper.get().Quotestatus.get().equals("To be Priced"))
		{
			//Need to write the codeExceptionPPT()
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Exception PPT");
			Configurationhelper.get().ExceptionPPT(Data);
			
			//C4Chelper.get().VerifyQuoteStage();
		}
		
		//Copy Line Item Features after quotes is in Price stage
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Copy line item");
		Configurationhelper.get().CopyLineItem(Data);
		ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Existing in the Method from copy line item! "); 
		
		//Copy Quote features after Quote is in Price stage
		
		if(!Data[0][64].toString().equals(""))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Copy Quote");
			C4Chelper.get().CopyQuoteItem(Data);
		}
		else {
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Proceeding with Same Quote!!!!");
		}
		if(Data[0][1].toString().equals("")&& !Data[0][73].toString().equalsIgnoreCase("Yes"))
		{
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Price Look up quote has been successfully Priced");
		}
	
		else
		{
			// If Stage is waiting for third Party Need to call All the Explore functions
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Bespoke");
			BspokeNonStandard.get().Bespoke(Data);
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method General Information");
			GenralInfohelper.get().GenralInfomration(Data);
		
			//Login.get().OpenCPQQuoteDirectly();
			ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method BCN Update");
			BCNupdatehelper.get().BCNUpdate(Data);
			//DisscountAndAprrovalhelper.get().DisscountandApprove(Data);
		
		
			if(Data[0][21].toString().contains("Quote Level"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Apply Discount Quote Level");
				DisscountAndAprrovalhelper.get().ApplyDisscountQuotelevel(Data);
				
			}
			else if(Data[0][21].toString().contains("Line Level"))
			{
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Apply Discount Line level");
				DisscountAndAprrovalhelper.get().ApplyDisscountlinelevel(Data);
			}
			
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Approve Quote");
				DisscountAndAprrovalhelper.get().ApproveQuote(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Set Current Quote Stage");
				Configurationhelper.get().SetCurrectQuoteStage();
				//C4Chelper.get().VerifyQuoteStage();
				// belowMethods will not call if Quote is not in Aprroved Stage 
			if(Configurationhelper.get().Quotestatus.get().equals("Approved"))
			{
				GenralInfohelper.get().AdminChanges(Data);
				ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Customer Sign");
				SendProposalhelper.get().CustomerSign(Data);
				//C4Chelper.get().VerifyQuoteStage();
				if(Data[0][24].toString().equals("Email")) 
				{
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Accept Quote");
					Orderinghelper.get().AcceptsQuote(Data);
					//C4Chelper.get().VerifyQuoteStage();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Create Order");
					Orderinghelper.get().CreateOrder(Data);
					
					//Kashyap
//					Orderinghelper.get().CaptureServiceOrder(Data);
					//C4Chelper.get().VerifyQuoteStage();
			    }
			    else 
			    {
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Accept Quote By E-Signature");
					Orderinghelper.get().AcceptsQuotebyEsignature(Data);
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Navigate To C4C");
					C4Chelper.get().NavigatetoC4C();
		//			C4Chelper.get().Movetoaccount(Data);
		//			C4Chelper.get().MovetoOpportunuity(Data);
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Edit Quote");
					C4Chelper.get().EditQuote();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Check Document Signed ");
					C4Chelper.get().CheckdocumentSigned();
					//C4Chelper.get().VerifyQuoteStage();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Accept Quote");
					Orderinghelper.get().AcceptsQuote(Data);
					//C4Chelper.get().VerifyQuoteStage();
					ExtentTestManager.getTest().log(LogStatus.PASS, " Step: Entering in the Method Create Order");
					Orderinghelper.get().CreateOrder(Data);
					//C4Chelper.get().VerifyQuoteStage();
//					Orderinghelper.get().CaptureServiceOrder(Data);
				
			    }	
		      }
		   }
			
		//Configurationhelper.get().AddProducttest(Data);
			//Login.get().Login("Siebel");
			//Orderinghelper.get().SeibleOrderVerification(Data);
		
	}

@Test(dataProviderClass=DataReader.class,dataProvider="NewStandrdOrder")
public void Testermethod(Object[][] Data) throws Exception
{
	
	C4Chelper.get().LoginAsC4CAddmin("C4C");
	C4Chelper.get().OpenOpportunutiDetail();
	C4Chelper.get().UpdateOpportunuty(Data);
	//C4Chelper.get().Logout();
}

@Test(dataProviderClass=DataReader.class,dataProvider="NewContainer")
public void EndtoEndOrderContainerNew(Object[][] Data) throws Exception
{
	Login.get().Login("C4C");
	
	C4Chelper.get().Movetoaccount(Data);
	C4Chelper.get().MovetoOpportunuityContainer(Data);
	
	Thread.sleep(3000);
	C4Chelper.get().Product_Add();
	C4Chelper.get().AddContainerQuote(Data);
	ContainerHelper.get().AddContainerProduct(Data);
	//C4Chelper.get().VerifyQuoteStage();
	GenralInfohelper.get().GenralInfomration(Data);
	ContainerHelper.get().ContainerApproveQuote();
	if(!Data[0][4].equals("CST")) {
	ContainerHelper.get().ContainerSEApproval();
	//C4Chelper.get().VerifyQuoteStage();
	ContainerHelper.get().ContainerCSTApproval(Data);
	//C4Chelper.get().VerifyQuoteStage();
}
	else
	{
		ContainerHelper.get().ContainerCSTApproval(Data);
	}
	SendProposalhelper.get().Containerprices(Data);
	SendProposalhelper.get().CustomerSign(Data);
	if(Data[0][24].toString().equals("Email")) {
		Orderinghelper.get().AcceptsQuote(Data);
		ContainerHelper.get().ContainerCreateOrder();
	//	Orderinghelper.get().CreateOrder(Data);
	}
	else {
		Orderinghelper.get().AcceptsQuotebyEsignature(Data);
		C4Chelper.get().NavigatetoC4C();
		//C4Chelper.get().Movetoaccount(Data);
		//C4Chelper.get().MovetoOpportunuity(Data);
		C4Chelper.get().EditQuote();
		C4Chelper.get().CheckdocumentSigned();
		Orderinghelper.get().AcceptsQuote(Data);
		ContainerHelper.get().ContainerCreateOrder();
		
		}
	
	
}
@Test(dataProviderClass=DataReader.class,dataProvider="Bulkupload")
public void EndtoEndOrderBulkUpload(Object[][] Data) throws Exception
{
	//ExtentTestManager.getTest().setDescription("Login Into C4C");

	Login.get().Login("C4C");
	
	C4Chelper.get().Movetoaccount(Data);
	C4Chelper.get().MovetoOpportunuity(Data);
	
	Thread.sleep(3000);
	C4Chelper.get().AddQuote();
	//Login.get().OpenCPQQuoteDirectly();
	BulkHelper.get().AddProductWithbulkTemplate(Data);
	if(Configurationhelper.get().Quotestatus.get().equals("POA"))
	{
		Configurationhelper.get().POA();
		C4Chelper.get().VerifyQuoteStage();
		}
	else if(Configurationhelper.get().Quotestatus.get().equals("Created"))
	{
		Configurationhelper.get().SEDataupdate(Data);
		C4Chelper.get().VerifyQuoteStage();
	}
	else if(Configurationhelper.get().Quotestatus.get().equals("To be Priced"))
	{
		//Need to write the codeExceptionPPT()
		Configurationhelper.get().ExceptionPPT(Data);
		C4Chelper.get().VerifyQuoteStage();
	}
	// If Stage is waiting for third Party Need to call All the Explore functions
	
	GenralInfohelper.get().GenralInfomration(Data);
	BCNupdatehelper.get().BCNUpdate(Data);
	//DisscountAndAprrovalhelper.get().DisscountandApprove(Data);
	
	
	if(Data[0][21].toString().contains("Quote Level")){
		DisscountAndAprrovalhelper.get().ApplyDisscountQuotelevel(Data);
		
	}
	else if(Data[0][21].toString().contains("Line Level"))
	{
		DisscountAndAprrovalhelper.get().ApplyDisscountlinelevel(Data);
	}
	DisscountAndAprrovalhelper.get().ApproveQuote(Data);
	Configurationhelper.get().SetCurrectQuoteStage();
	// belowMethods will not call if Quote is not in Aprroved Stage 
	if(Configurationhelper.get().Quotestatus.get().equals("Approved")){
	SendProposalhelper.get().CustomerSign(Data);
	C4Chelper.get().VerifyQuoteStage();
	if(Data[0][24].toString().equals("Email")) {
		Orderinghelper.get().AcceptsQuote(Data);
		C4Chelper.get().VerifyQuoteStage();
		Orderinghelper.get().CreateOrder(Data);
		C4Chelper.get().VerifyQuoteStage();
	}
	else {
		Orderinghelper.get().AcceptsQuotebyEsignature(Data);
		C4Chelper.get().NavigatetoC4C();
		C4Chelper.get().Movetoaccount(Data);
		C4Chelper.get().MovetoOpportunuity(Data);
		C4Chelper.get().EditQuote();
		C4Chelper.get().CheckdocumentSigned();
		C4Chelper.get().VerifyQuoteStage();
		Orderinghelper.get().AcceptsQuote(Data);
		C4Chelper.get().VerifyQuoteStage();
		Orderinghelper.get().CreateOrder(Data);
		C4Chelper.get().VerifyQuoteStage();
		
		}
	
}
		
//Configurationhelper.get().AddProducttest(Data);
	Login.get().Login("Siebel");
	Orderinghelper.get().SeibleOrderVerification(Data);
	
}

@Test(dataProviderClass=DataReader.class,dataProvider="ProductModel")
public void ProductmodelRules(Object[][] Data) throws Exception

{
		Login.get().Login("C4C");
       C4Chelper.get().Movetoaccount(Data);
       C4Chelper.get().MovetoOpportunuity(Data);
       Thread.sleep(3000);
       C4Chelper.get().AddQuote();
       PrductModelHelper.get().AddProductrule(Data);
}



}
